module.exports = {
  'FastInit': { 'exports': 'global:FastInit' },
  'LOCALE_SETTINGS' : { 'exports': 'global:LOCALE_SETTINGS' },
  'head' : { 'exports': 'global:head' },
  'lightbox' : { 'exports': 'global:lightbox' }
};
