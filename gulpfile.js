var gulp = require('gulp');
var runSequence = require('run-sequence');
var clean = require('gulp-clean');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var zip = require('gulp-zip');

gulp.task('clean', function() {
  return gulp.src(['dist', "./cesu-bb-ux-ch.zip"], {
		read: false
	})
	.pipe(clean());
});

gulp.task('build', function() {
  return runSequence('clean', 'scripts');
});

gulp.task('scripts', function() {
  return bundleJs("./src/scripts/cesu.js", './dist');
});

var bundleJs = function(src, dest) {
  return getBrowserifyBundler(src)
  	.bundle()
  	.pipe(source('cesu-bb-ux-ch.js'))
  	.pipe(buffer())
  	.pipe(uglify())
  	.pipe(gulp.dest(dest));
};

var getBrowserifyBundler = function(src) {
  var bundler = browserify(src, {
    // Add file extentions to make optional in your requires
    extensions: ['.hbs'],
    debug: true,
    paths: ['./node_modules', './src/scripts']
  });

  bundler.transform('babelify', {
    presets: ['es2015']
  });

  bundler.transform('browserify-shim');
  bundler.transform('hbsfy');
  bundler.transform('stripify');

  return bundler;
};