This folder contains everything needed to understand, maintain, and continue development on the project.




In this folder:

- Installation files



In js_hack:

- Installation files
- LMS Intention
	- A form that was submitted, informing the LMS production team of the wish to install the building block.



In documentation:

- Project Description
	- Overall description of the project, incl. research data, design decisions, etc.
- LMS Intention
	- A form that was submitted, informing the LMS production team of the intention to develope the project.
- Handover Checklist
	- Details the procedure used in the handover process.
- Installation Manual
	- Incl. instructions for installation, updates, and uninstallation.
- Test Documention
	- Documentaion of concluded test, as well as suggestions for future testing.
- User Manual
	- Detailed instruction on how users can interact with the project after installation.