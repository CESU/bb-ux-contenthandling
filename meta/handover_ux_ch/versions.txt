v1.0: First release candidate
v1.1: "Get token"-script changed name and functionality, so that it allows async callbacks. Fixed a number of bugs. Documentaiton updated to reflect the changes. Added new checklist for handover. Added new test.
v1.2: Fixed an issue that caused the "Insert Content Below" button to always insert the new Content Item above the old Content Item.