// Expose the utilities.
require('utilities/expose');

// required
require('vendor/event.simulate');
require('vendor/head.load');

// handlebars helpers
require('utilities/hbs-iflanguage');

// Utilities
require('utilities/logExceptions');

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// content pages
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// add content
require('cesu/addContent/addContentSingleButton');
// availability toggles
require('cesu/itemAvailabilityButton');
// move to top buttons
require('cesu/moveToTopButton');
// resize buttons for content items
require('cesu/collapseExpandButtons');

// show the date and time where a content item is available to students
require('cesu/showAvailabilityDates');

// allows users to download all attachments on a content item, or on an entire page
require('cesu/downloadItemAttachments');

// allows users to quickly acces the edit menu for individual content items
require('cesu/editButtons');

// allows users to double-click to edit texts for individual content items
require('cesu/editContentDirectly');

// adds a shortcut to toggle statistics tragging
require('cesu/statisticsToggleButton');

// adds shortcuts to show/hide menu items for courses
require('cesu/menuAvailabilityButton');