/*
What does this fix do?
1. compliments Bb's content creation flow with more granular/clear flow
single "+" button
2. add inline content insertion buttons to content items

SOLUTION
*/

/* jshint multistr:true */
/* global lightbox */
// jscs:disable

var _ = require('underscore');
var addFix = require('utilities/addFix');
var getElement = require('utilities/getElement');
var getElements = require('utilities/getElements');
var htmlToElement = require('utilities/htmlToElement');
var styleTag = require('./addContentSingleButtonStyles')();
var language = require('utilities/language');
var urlContains = require('utilities/urlContains');
var courseId = require('utilities/courseId');
var contentId = require('utilities/contentId');
var moveItem = require('utilities/moveItem');
var addUserAction = require('utilities/addUserAction');
var templateWarningDialog = require('./warningDialog');

var cookieName = 'tol_insert_item_data_' + courseId + '_' + contentId;

// find object by attribute in array
var arrayContainsObj = function (arrayToSearch, attribute, value) {
  var ind;
  var cnt = 0;
  arrayToSearch.each(function(element) {
    if (element[attribute] === value) {
      ind = cnt;
      return false;
    }
    cnt += 1;
  });
  return ind;
};

// fix to be applied
var replaceAddContentButtonSet = function replaceAddContentButtonSet() {

  // add styles for new buttons and modal
  var addContentStyles = new Element('style');
  addContentStyles.id = 'addContentStyles';
  getElement('body').insert({ top: addContentStyles });
  addContentStyles.innerHTML = styleTag;

  // re-organize Blackboard's content types
  var availableContentTypes = [];
  var processedContentTypes = [];

  // different categories of content types
  // primary: displayed as stand-alone buttons, not in list or menu
  // different for x-toledo...
  var primaryContentTypes = [
    'content-handler-resource/x-bb-document',
    'content-handler-resource/x-bb-folder'
  ];

  // 'plain' content types, will be displayed in separate list or menu
  var baseContentTypes = [
    'content-handler-resource/x-bb-file',
    'content-handler-resource/x-bb-externallink',
    'content-handler-resource/x-bb-courselink',
    'content-handler-resource/x-bb-lesson',
    'content-handler-resource/x-bb-blankpage',
    'content-handler-resource/x-plugin-scormengine',
    'content-handler-resource/x-librilinks'
  ];
  // 'media' content types, will be displayed in separate list or menu
  var mediaContentTypes = [
    'content-handler-resource/x-bb-audio',
    'content-handler-resource/x-bb-image',
    'content-handler-resource/x-osv-kaltura',
    'content-handler-resource/x-bb-flickr-mashup',
    'content-handler-resource/x-bb-slideshare-mashup',
    'content-handler-resource/x-bb-youtube-mashup'
  ];
  // 'evaluation' content types, will be displayed in separate list or menu
  var assessmentContentTypes = [
    'content-handler-resource/x-bb-asmt-test-link',
    'content-handler-resource/x-bb-asmt-survey-link',
    'content-handler-resource/x-bb-assignment',
    'content-handler-resource/x-turnitin-proxy'
  ];

  // 'miscellaneous' content types, will be displayed in separate list or menu
  var toolContentTypes = [
    'content-handler-resource/x-bb-forumlink',
    'content-handler-resource/x-bb-bloglink',
    'content-handler-resource/x-bb-journallink',
    'content-handler-resource/x-bb-wiki',
    'content-handler-resource/x-bb-grouplink'
  ];

  // labels for menus/lists of content types are defined
  var baseContentLabelID = 'bcMenu_actionButton';
  var baseContentLabel;
  var mediaContentLabel = 'Media';
  var assessmentContentLabelID = 'evaMenu_actionButton';
  var assessmentContentLabel;
  var toolContentLabelID = 'aitMenu_actionButton';
  var toolContentLabel;
  var extraContentLabel;
  
  switch (language) {
    case 'en':
      extraContentLabel = 'Extra'
      break;
    case 'da':
      extraContentLabel = 'Øvrige';
      break;
  }


  // get availableContentTypes from page, build array
  getElements('#actionbar > ul#nav > li > h2 > a').each(function(menuTitle) {
    // labels are extracted from page contentType
    // media was set manually
    switch (menuTitle.id) {
      case baseContentLabelID:
        baseContentLabel = menuTitle.innerHTML.stripTags().trim();
        break;
      case assessmentContentLabelID:
        assessmentContentLabel = menuTitle.innerHTML.stripTags().trim();
        break;
      case toolContentLabelID:
        toolContentLabel = menuTitle.innerHTML.stripTags().trim();
        break;
    }
    // array of available content types is populated
    menuTitle.up('li').select('div ul > li > a').each(function(contentType) {
      var contentTypeLabel = contentType.innerHTML.stripTags().trim();
      if (contentType.id && !arrayContainsObj(availableContentTypes, 'label', contentTypeLabel)) {
        availableContentTypes.push({
          id: contentType.id,
          label: contentTypeLabel
        });
      }
    });
  });

  // sort array of available content types
  availableContentTypes = _.sortBy(availableContentTypes, 'label');

  // prepare html for new content-modal
  var mainContentButtonsString = '';
  var counter;
  var contentType;
  var indexOfContentType;

  // primary content types are processed
  for (counter = 0; counter < primaryContentTypes.length; counter += 1) {
    contentType = primaryContentTypes[counter];
    indexOfContentType = arrayContainsObj(availableContentTypes, 'id', contentType);
    if (indexOfContentType > -1) {
      mainContentButtonsString += '<li><a class="contentTypeLink" contentType ="' + contentType + '">' + availableContentTypes[indexOfContentType].label + '</a></li>';
      availableContentTypes.splice(indexOfContentType, 1);
    }
  }

  // function to build sub-menus of content types
  var processNamedGroupOfContentTypes = function(nameOfGroup, labelOfGroup, arrayOfTypes) {
    var returnString = '';
    for (counter = 0; counter < arrayOfTypes.length; counter += 1) {
      contentType = arrayOfTypes[counter];
      indexOfContentType = arrayContainsObj(availableContentTypes, 'id', contentType);
      if (indexOfContentType > -1) {
        if (mainContentButtonsString.indexOf(nameOfGroup) === -1) {
          mainContentButtonsString += '<li><a class="contentToggler fas ' + nameOfGroup + '" toggle="' + nameOfGroup + '">' + labelOfGroup + '</a></li>';
        }
        returnString += '<li><a class="contentTypeLink" contentType="' + contentType + '">' + availableContentTypes[indexOfContentType].label + '</a></li>';
        availableContentTypes.splice(indexOfContentType, 1);
      }
    }
    return returnString;
  };

  // construct sub-menus of content items
  var baseContentButtonsString = '';
  // build other lists (do not do this in x-toledo)
  var mediaContentButtonsString = '';
  var assessmentContentButtonsString = '';
  var toolContentButtonsString = '';
  var extraContentButtonsString = '';

  baseContentButtonsString = processNamedGroupOfContentTypes('baseContent', baseContentLabel, baseContentTypes);
  mediaContentButtonsString = processNamedGroupOfContentTypes('mediaContent', mediaContentLabel, mediaContentTypes);
  assessmentContentButtonsString = processNamedGroupOfContentTypes('assessmentContent', assessmentContentLabel, assessmentContentTypes);
  toolContentButtonsString = processNamedGroupOfContentTypes('toolContent', toolContentLabel, toolContentTypes);
  // push remaining content types into the "extra" or "miscellaneous" sub-menu
  availableContentTypes.each(function(contentType) {
    if (processedContentTypes.indexOf(contentType) === - 1) {
      if (mainContentButtonsString.indexOf('extraContent') === -1) {
        mainContentButtonsString += '<li><a class="contentToggler fas extraContent" toggle="extraContent">' + extraContentLabel + '</a></li>';
      }
      extraContentButtonsString += '<li><a class="contentTypeLink" contentType="' + contentType.id + '">' + contentType.label + '</a></li>';
    }
  });
  

  // prepare html to be inserted
  var htmlString = '\<div id="insertNewContentContainerBG" style="display: none">\
      <div id="insertNewContentContainer">\
      <ul id="mainContentButtons">' + mainContentButtonsString + '</ul>\
      <div id="listNewContentTypes" style="display: block">\
        <ul id="baseContent" style="display: none">' + baseContentButtonsString + '</ul>\
        <ul id="mediaContent" style="display: none">' + mediaContentButtonsString + '</ul>\
        <ul id="assessmentContent" style="display: none">' + assessmentContentButtonsString + '</ul>\
        <ul id="toolContent" style="display: none">' + toolContentButtonsString + '</ul>\
        <ul id="extraContent" style="display: none">' + extraContentButtonsString + '</ul>\
      </div>\
    </div>\
  </div>';

  // create new DOM element
  var addContentContainer = htmlToElement(htmlString);

  // add new container for content buttons
  if (getElement('#actionbar')) {

    getElement('body').insert({
        bottom: addContentContainer
    });
  }

  // function to display sub-menus of content types
  var toggleContentTypes = function(contentGroupClassName) {
    getElements('#listNewContentTypes > ul').each(function(listContentTypes) {
      if (listContentTypes.id !== contentGroupClassName) {
        listContentTypes.hide();
      } else {
        if (listContentTypes.visible()) {
          listContentTypes.hide();
        } else {
          listContentTypes.show();
        }
      }
    });
  };

  var toggleModal = function(triggeredBy) {
    lightbox.closeCurrentLightbox();
    var insertModal = getElement('#insertNewContentContainerBG');
    console.log(insertModal.readAttribute('style'));
    // if modal is already visible > hide it
    // remove noscroll from body
    if (insertModal.visible()) {
      insertModal.hide();
      getElement('body').removeClassName('noScroll');
    } else {
      // contextual insert > set scope for modal
      if (triggeredBy.match('.tolInsertBefore') || triggeredBy.match('.tolInsertAfter')) {
        insertModal.setAttribute('scope', 'inline');
        insertModal.setAttribute('insertWhere', triggeredBy.readAttribute('class').match("\\btolInsertAfter|tolInsertBefore\\b")[0].replace('tolInsert', '').toLowerCase());
        insertModal.setAttribute('refItem', triggeredBy.up('.liItem').down('.item').id);
      } else {
        insertModal.setAttribute('scope', false);
        insertModal.setAttribute('insertWhere', false);
        insertModal.setAttribute('refItem', false);
      }
      // modal is not yet visible
      // add noscroll to body, modal is positioned in it's logical place, if user could scroll, modal would appear out of place
      getElement('body').addClassName('noScroll');
      // to calculate height and width of modal, it must be displayed, hide it using visibility
      insertModal.setStyle({
        visibility: 'hidden'
      });
      insertModal.show();
      console.log(insertModal.readAttribute('style'));
      // to calculate position submenus in the modal must be expanded
      getElements('#listNewContentTypes > ul').each(function(listContentTypes) {
        listContentTypes.show();
      });
      // make modal as wide as .locationPane
      // make modal as high as window
      var modalWidth = getElement('.locationPane').getDimensions().width + 'px';
      var modalHeight = document.viewport.getDimensions().height + 'px';
      var leftPosition = getElement('.locationPane').cumulativeOffset().left;
      insertModal.style.width = modalWidth;
      insertModal.style.height = modalHeight;
      insertModal.style.left = leftPosition + 'px';

      // position new modal's content in the vertical center of the modal
      // offset is calculated
      var contentElement = insertModal.down('#insertNewContentContainer');
      var centerTopPosition;
      centerTopPosition = (insertModal.getDimensions().height / 2) - (contentElement.getDimensions().height / 2) + 'px';
      // offset is applied
      contentElement.style.marginTop = centerTopPosition;

      // hide subsections in the modal
      getElements('#listNewContentTypes > ul').each(function(ul) {
        ul.hide();
      });

      // reset layout of main buttons in the modal
      getElements('#mainContentButtons .contentToggler').each(function(toggler) {
        toggler.removeClassName('active');
      });

      // show the modal
      insertModal.setStyle({
        visibility: 'visible'
      });
    }
  };

  // observe user actions within the modal:
  addContentContainer.observe('click', function(event) {
    if (event.target.hasClassName('contentToggler')) {
      toggleContentTypes(event.target.readAttribute('toggle'));
      getElements('.contentToggler').each(function(toggler) {
        if (event.target.readAttribute('toggle') === toggler.readAttribute('toggle')) {
          if (event.target.hasClassName('active')) {
            event.target.removeClassName('active');
          } else {
            event.target.addClassName('active');
          }
        } else {
          toggler.removeClassName('active');
        }
      });
    }
    if (event.target.hasClassName('contentTypeLink')) {
      // if cookie existed: unset old cookie
      if (localStorage.getItem(cookieName)) {
        localStorage.removeItem(cookieName);
      }
      // depending on scope: set new cookie
      if (addContentContainer.readAttribute('scope') === 'inline') {
        var seconds = new Date().getTime() / 1000;
        seconds = Math.floor(seconds);
        var numItems = getElements('#content_listContainer > .liItem').length;
        var itemPosition = addContentContainer.readAttribute('insertWhere');
        var refItem = addContentContainer.readAttribute('refItem');
        var itemList;
        getElements('#content_listContainer > .liItem .item').each(function (itemInList) {
          itemList += itemInList.id + ';';
        });
        // set cookie
        var cookieValues = {
          timestamp: seconds,
          course_id: courseId,
          parent_id: contentId,
          item_position: itemPosition,
          ref_item: refItem,
          num_items: numItems,
          item_list: itemList
        };
        localStorage.setItem(cookieName, JSON.stringify(cookieValues));
      }
      if (getElement('#' + event.target.readAttribute('contentType'))) {
        getElement('#' + event.target.readAttribute('contentType')).simulate('click');
      }
    }
  });

  // add 'plus' button to every item on the page
  //var plusTitle = {'nl' : 'Voeg inhoud toe op deze positie', 'en': 'Insert new content at this position'};
  getElements('#content_listContainer > li').each(function(listItem) {
    var newButton = {};
    newButton.title_nl = 'Voeg inhoud toe op deze positie';
    newButton.title_en = 'Insert new content at before this element';
    newButton.title_da = 'Indsæt nyt element ovenfor'
    newButton.class = 'tolInsertBefore fas';
    addUserAction(listItem, newButton);
    newButton.title_en = 'Insert new content at after this element';
    newButton.title_da = 'Indsæt nyt element nedenfor'
    newButton.class = 'tolInsertAfter fas';
    addUserAction(listItem, newButton);
  });

  // assign behaviour to buttons and user clicks
  getElement('body').observe('click', function(event) {
    if (event.target.match('.tolInsertBefore') || event.target.match('.tolInsertAfter')) {
      // if contextual menu is open, do not open the modal
      if (!getElement('.cmdiv')) {
        toggleModal(event.target);
      }
    } else if (event.target.match('#insertNewContentContainerBG')) {
      if (getElement('#insertNewContentContainerBG').visible() && !event.target.up('#insertNewContentContainer')) {
        getElement('#insertNewContentContainerBG').hide();
        getElement('body').removeClassName('noScroll');
      }
    }
  });

  // when resizing the page: remove modal
  Event.observe(window, 'resize', function() {
    if (getElement('#insertNewContentContainerBG').visible()) {
      getElement('#insertNewContentContainerBG').hide();
      getElement('body').removeClassName('noScroll');
    }
  });

  // function that performs the actual move of the new item
  // on the page
  // in the database
  var executeMove = function(elementsFromCookie) {
    var targetPosition;
    // calculate position of reference item
    getElements('#content_listContainer > .liItem').each(function(liItem, index) {
      // if current list-item is reference item
      // new position can be calculated
      // new item can be moved
      if (liItem.id === 'contentListItem:' + elementsFromCookie.ref_item) {
        if (elementsFromCookie.item_position === 'after') {
          targetPosition = index + 1;
          // move item on screen
          liItem.insert({
            after: getElement('#' + lastItemOnPage)
          });
        } else {
          targetPosition = index;
          // move item on screen
          liItem.insert({
            before: getElement('#' + lastItemOnPage)
          });
        }
        moveItem(getElement('#' + lastItemOnPage), targetPosition);
        // unset cookie
        localStorage.removeItem(cookieName);
      }
    });
  };

  // when returning on the page: move item to selected position
  // check for existence of cookie, if it exists, last item will be moved to requested position
  if (localStorage.getItem(cookieName)) {
    // read cookie into object
    var elementsFromCookie = JSON.parse(localStorage.getItem(cookieName));
    // check for success-receipt
    if (getElement('#inlineReceipt_good')) {
      console.log('cookie found!');
      // last item on page should be moved
      var lastItemOnPage = getElements('#content_listContainer > .liItem').last().id;
      console.log('last item on page: ' + lastItemOnPage);
      // check whether this truly is new item
      // was it on the page the first time around?
      // if last item already was on the page > do nothing
      if (elementsFromCookie.item_list.indexOf(lastItemOnPage + ';') === -1) {
        console.log('last item is new!');
        // check whether reference item is still present
        if (getElement('#contentListItem:' + elementsFromCookie.ref_item)) {
          console.log('ref item is present!');
          // if page contents changed after the operation started, this will be set to false
          var validCookie = true;
          // check whether number of items = old number of items + 1
          console.log('old number of items on this page: ' + elementsFromCookie.num_items);
          console.log('new number of items on this page: ' + getElements('#content_listContainer > .liItem').length);
          if (parseInt(elementsFromCookie.num_items) !== (getElements('#content_listContainer > .liItem').length - 1)) {
            validCookie = false;
            console.log('number of items on the page has changed!');
          } else {
            console.log('number of items is unchanged!');
          }
          if (validCookie) {
            executeMove(elementsFromCookie);
          } else {
            // show warning
            console.log('caution! the page has changed!');
            // depending on answer, move item or leave item
            var pageChangedWarningTitle = {
              'en': 'Caution!',
              'nl': 'Opgelet',
              'da': 'Advarsel!'
            };
            var pageChangedWarningMsg = {
              'en': 'This page has changed since you initiated the contextual content insertion. Do you still want to move the newly created item to the requested position?',
              'nl': 'De pagina is veranderd sinds je begon met het aanmaken van het nieuwe item. Wil je het nieuwe item nog steeds naar de gevraagde positie verplaatsen?',
              'da': 'Siden har ændret sig siden du begyndte at indsætte nyt indhold. Vil du stadigvæk flytte det nylavede element til den ønskede position?'
            };
            var pageChangedCancelLabel = {
              'nl': 'Niet verplaatsen',
              'en': 'Do not move',
              'da': 'Flyt ikke'
            };
            var pageChangedContinueLabel = {
              'nl': 'Verplaatsen',
              'en': 'Move',
              'da': 'Flyt'
            };
            var html = templateWarningDialog({
              warningMSG: pageChangedWarningMsg[language],
              cancelButton: pageChangedCancelLabel[language],
              okButton: pageChangedContinueLabel[language]
            });
            var pageChangedWarning = new lightbox.Lightbox({
              lightboxId: 'pageChangedWarning',
              title: pageChangedWarningTitle[language],
              contents: html,
              defaultDimensions: { w: 420, h: 220 },
              useDefaultDimensionsAsMinimumSize: true
            });
            var observeContinuePageChanged = function() {
              getElement('#pageChangedWarning').observe('click', function(event) {
                if (event.target.match('.okButton')) {
                  executeMove(elementsFromCookie);
                }
              });
            };
            pageChangedWarning.open(observeContinuePageChanged);
          }
        } else {
          // show message "the reference item no longer is present..."
          console.log('the reference item no longer is present...');
        }
      }
    } else {
      // check timestamp: if older than 1 hour: remove cookie
      var seconds = new Date().getTime() / 1000;
      seconds = Math.floor(seconds) - 3600;
      if (elementsFromCookie.timestamp < seconds) {
        localStorage.removeItem(cookieName);
      }
    }
  }
};

addFix(replaceAddContentButtonSet, 'listContentEditable.jsp', '#controlPanelPalette');