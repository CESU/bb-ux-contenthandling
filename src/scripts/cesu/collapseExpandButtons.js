/*
What does this fix do?
Blackboard offers collapse and expand buttons for items, but only in edit mode
this fix will add a set of collapse and expand buttons for:
- all individual items and announcements in overviews
- all items at once

SOLUTION
*/

// jscs:disable maximumLineLength

var getElement = require('utilities/getElement');
var getElements = require('utilities/getElements');
var addFix = require('utilities/addFix');
var addStyle = require('utilities/addStyle');
var maximizeButtonStyles = require('./collapseExpandButtonsStyles');
var addUserAction = require('utilities/addUserAction');
var urlContains = require('utilities/urlContains');

var addResizeButtons = function (elementsSelector, itemButtons, titleButtons) {
  addStyle(maximizeButtonStyles);
  // modify display of items
  // add button set to content items
  getElements(elementsSelector).each(function(item) {
    if (item.down('.details')) {
      if (
        item.down('.details').innerHTML.stripTags().trim().length > 0 ||
        item.down('.details').select('iframe', 'img', 'embed', 'object', 'video', 'source').length > 0
      ) {
        // insert buttons
        itemButtons.each(function(button) {
          addUserAction(item, button);
        });
        // observe buttons
        item.observe('click', function(event) {
          if (event.target.match('a.fullScreenItem') || event.target.hasClassName('restoreItem')) {
            /* if contextual menu is open, do not maximize the item */
            if (!getElement('.cmdiv') || !getElement('.cmdiv').visible()) {
              getElement('body').toggleClassName('fullScreenItemContainer');
              event.target.up('li').toggleClassName('fullScreenItem');
              // scroll naar top van item
              event.target.up('li').scrollTo();
            }
          }	else if (event.target.hasClassName('collapseItem') || event.target.hasClassName('expandItem')) {
            event.target.up('li').toggleClassName('collapsedItem');
          }
        });
      }
    }
  });

  // insert buttons in titlebar
  if (getElements(elementsSelector + ' .userActions > a').length > 0) {
    titleButtons.each(function(titleButton) {
      addUserAction(getElement('#pageTitleDiv'), titleButton);
    });
    // observe buttons in titlebar
    getElement('#pageTitleDiv').observe('click', function(event) {
      if (event.target.hasClassName('collapseAll')) {
        getElements('.collapseItem').each(function(collapser) {
          collapser.up('li').addClassName('collapsedItem');
        });
      } else if (event.target.hasClassName('expandAll')) {
        getElements('.collapseItem').each(function(collapser) {
          collapser.up('li').removeClassName('collapsedItem');
        });
      }
    });
  }
};

var addContentItemResizeButtons = function() {
  var buttons = [];
  buttons.push({'class': 'restoreItem fas', 'title_nl': 'Klik om terug te keren naar de volledige pagina', 'title_en': 'Click to return to the page', 'title_da': 'Klik for at gendanne siden'});
  buttons.push({'class': 'fullScreenItem fas', 'title_nl': 'Klik om enkel dit item te bekijken (of af te drukken)', 'title_en': 'Click to view (or print) only this item', 'title_da': 'Klik for at se (eller printe) elementet i fuldskærm'});
  buttons.push({'class': 'collapseItem fas', 'title_nl': 'Klik om de inhoud van dit item dicht te klappen', 'title_en': 'Click to collapse the content of this item', 'title_da': 'Vis mindre'});
  buttons.push({'class': 'expandItem fas', 'title_nl': 'Klik om de inhoud van dit item open te klappen', 'title_en': 'Click to expand the content of this item', 'title_da': 'Vis mere'});
  var titleButtons = [
    {
      'class': 'collapseAll fas',
      'title_nl': 'Klik om de inhoud van alle items op deze pagina dicht te klappen',
      'title_en': 'Click to collapse the content of all items on this page',
      'title_da': 'Klik for at minimere alle elementer på siden'
    },
    {
      'class': 'expandAll fas',
      'title_nl': 'Klik om de inhoud van alle items op deze pagina open te klappen',
      'title_en': 'Click to expand the content of all items on this page',
      'title_da': 'Klik for at udvide alle elementer på siden'
    }
  ];

  // exception for wiki's overview
  var selector = 'ul#content_listContainer > li[id*="contentListItem:"]';
  if (urlContains('wikiList')) {
    selector = 'ul#content_listContainer > li';
  }
  addResizeButtons(selector, buttons, titleButtons);
};

// add buttons to content items
addFix(addContentItemResizeButtons, 'listContent', '');