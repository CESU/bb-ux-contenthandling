var addFix = require('utilities/addFix');
var checkContentType = require('utilities/checkContentTypeSupported');
var courseId = require('utilities/courseId');
var getElement = require('utilities/getElement');
var getElements = require('utilities/getElements');
var addUserAction = require('utilities/addUserAction');
var addStyle = require('utilities/addStyle');
var downloadButtonStyles = require('./downloadButtonsStyles');

const elementsSelector = 'ul#content_listContainer > li[id*="contentListItem:"]';

var getItemAttachments = function (contentItem) {
	var downloadRequests = [];

	var id = contentItem.id.replace('contentListItem:', '');
	var url = '//' + window.location.host + '/learn/api/public/v1/courses/' + courseId + '/contents/' + id + '/attachments';

	var request = new XMLHttpRequest();

	request.open('GET', url);
	request.setRequestHeader('Content-Type', 'application/json');

	request.onload = function() {
		switch (request.status) {
			case 200:
				var response = JSON.parse(request.responseText);				
				var attachments = response.results;

				attachments.each(function(attachment) {
					downloadRequests.push({"contentId": id, "attachmentId": attachment.id, "fileName": attachment.fileName});
				});

				downloadAll(downloadRequests);
			break;
			default:
				console.log('Something went wrong. Try again, or contact your local system administrator.');
			break;
		}
	};

	request.send();
};

var getPageAttachments = function () {

	getElements(elementsSelector).each(function(item) {

		if (item.down('.attachments')) {

			// If the items content-handler is supported in the blackboard api, it's attachments are queued for download.
			checkContentType(item, function(item) {
				let downloadRequests = [];
				let id = item.id.replace('contentListItem:', '');
				let url = '//' + window.location.host + '/learn/api/public/v1/courses/' + courseId + '/contents/' + id + '/attachments';
				
				let request = new XMLHttpRequest();	

				request.open('GET', url);
				request.setRequestHeader('Content-Type', 'application/json');

				request.onload = function() {
					switch (request.status) {
						case 200:
							let response = JSON.parse(request.responseText);				
							let attachments = response.results;

							attachments.each(function(attachment) {
								downloadRequests.push({"contentId": id, "attachmentId": attachment.id, "fileName": attachment.fileName});
							});

							downloadAll(downloadRequests);
						break;
						default:
							console.log('Something went wrong. Try again, or contact your local system administrator.');
						break;
					}
				};

				request.send();
			});
		};		
	});	
};

var downloadAll = function (downloadRequests) {

	downloadRequests.each(function(request) {

		let url = '//' + window.location.host + '/learn/api/public/v1/courses/' + courseId + '/contents/' + request.contentId + '/attachments/' + request.attachmentId + '/download';

		// Inspired by http://www.alexhadik.com/blog/2016/7/7/l8ztp8kr5lbctf5qns4l8t3646npqh
		let xhr = new XMLHttpRequest();

		xhr.open('GET', url);
		xhr.responseType = 'blob';

		xhr.onload = function() {
			switch (xhr.status) {
				case 200:
					let blob = new Blob([this.response]);

					if (window.navigator && window.navigator.msSaveBlob) {
						//For Internet Explorer use this to download file since blobs need to be saved first
						window.navigator.msSaveBlob(blob, request.fileName);
					} else {
						//Create a link element, hide it, direct 
						//it towards the blob, and then 'click' it programatically
						//and remove it again
						let a = document.createElement('a');

						a.href = window.URL.createObjectURL(blob);
						a.download = request.fileName;
						a.style.cssText = "display: none";

						document.body.appendChild(a);

						a.click();

						document.body.removeChild(a);
					}


					break;
				default:
					alert('Something went wrong downloading a file. Try again, or contact your local system administrator.');
				break;
			}
		};

		xhr.send();
	});
}

var addDownloadButtons = function () {

	// modify display of items
	// add button set to content items
	getElements(elementsSelector).each(function(item) {

		if (item.down('.attachments')) {

			// Adds the download button if the items content-handler is supported in the blackboard api.
			checkContentType(item, function(item) {

				addUserAction(item, {
					'class': 'downloadItemBtn fas',
					'title_en': 'Click to download attachments for this item',
					'title_da': 'Klik for at downloade alle vedhæftninger på dette element'
				});

				// observe buttons
				item.down('.downloadItemBtn').observe('click', function (event) {
					getItemAttachments(event.target.up('li'));
				});
			});
		}
		
	});

	// insert buttons in titlebar
	if (getElements(elementsSelector + ' .attachments').length > 0) {

		addUserAction(getElement('#pageTitleDiv'), {
			'class': 'downloadPageBtn fas',
			'title_en': 'Click to download all attachments for this page',
			'title_da': 'Klik for at downloade alle vedhæftninger på hele siden'
		});

		// observe buttons in titlebar
		getElement('#pageTitleDiv').observe('click', function(event) {
			if (event.target.hasClassName('downloadPageBtn')) {
				getPageAttachments();
			}
		});
	}
};

addStyle(downloadButtonStyles);
addFix(addDownloadButtons, 'listContent');