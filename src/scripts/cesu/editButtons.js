var addFix = require('utilities/addFix');
var courseId = require('utilities/courseId');
var getElement = require('utilities/getElement');
var getElements = require('utilities/getElements');
var addUserAction = require('utilities/addUserAction');
var addStyle = require('utilities/addStyle');
var editButtonStyles = require('./editButtonsStyles');

const elementsSelector = 'body[class*="ineditmode"] ul#content_listContainer > li[id*="contentListItem:"]';

var addEditButtons = function () {

	// modify display of items
	// add button set to content items
	getElements(elementsSelector).each(function(item) {

		addUserAction(item, {
			'class': 'editItemBtn fas',
			'title_en': 'Click to edit this item',
			'title_da': 'Klik for at redigére elementet'
		});

		// observe buttons
		item.down('.editItemBtn').observe('click', function (event) {
			goToEdit(event.target.up('li'));
		});
		
		
	});
};

function goToEdit(item) {
	// Get the context dropdown and click it
	var contextDropper = item.down('a[id*="cmlink"]').simulate('focus').simulate('click');

	let contexMenu;

	while(!contexMenu) {
		// Get the resulting context menu 
		contexMenu = getElement('body div[id^="cmdiv"]');
	}

	let timer = setTimeout(function() {
		// Click the edit anchor		
		contexMenu.down('ul[id^="cmul"] > li[id*="edit"] > a').simulate('click');
	}, 200);
}

addStyle(editButtonStyles);
addFix(addEditButtons, 'listContentEditable.jsp');