var addFix = require('utilities/addFix');
var courseId = require('utilities/courseId');
var getElement = require('utilities/getElement');
var getElements = require('utilities/getElements');
var doAuthorizedCall = require('utilities/doAuthorizedCalls');
var checkContentType = require('utilities/checkContentTypeSupported');
var addStyle = require('utilities/addStyle');
var editContentDirectlyStyles = require('./editContentDirectlyStyles');

const elementsSelector = 'body[class*="ineditmode"] ul#content_listContainer > li[id*="contentListItem:"]';

var addEditFeatures = function () {

	// Check each content item to see if they are editable.
	// If they are, the relevent events are added.
	// Else the elements are ignored.
	getElements(elementsSelector).each(function(item) {
		checkContentType(item, addEditEvents);
	});
};

// Add events to individual content items
function addEditEvents(item) {
	var itemTitle = item.down('h3 > :not([class*="reorder"])');
	var itemBody = item.down('div[class*="details"] > div[class*="vtbegenerated"]');

	//Title
	if (itemTitle) {
		var cont = false;
		var prevent = false;
		var timer = 0;


		itemTitle.addClassName('editable').observe('click', function(e) {
			if (!cont) {
				e.preventDefault();

				timer = setTimeout(function() {
					if (!prevent) {
						cont = true;
						e.target.click();
					}

					prevent = false;
				}, 400);

				cont = false;
			}
			
		})
		.observe('dblclick', function(e) {
			clearTimeout(timer);
			prevent = true;

			editTitle(e.target);
		});
	}

	//Body
	if (itemBody) {
		itemBody.addClassName('editable').observe('dblclick', function(e) {
			editBody(e.target);
		});			
	}
}

function editTitle(element) {
	var parent = element.up();
	var parentHref = parent.href;
	let oldTitle = element.innerHTML;

	// Remove the href of parent element to prevent unwanted redirects and showing of link status
	parent.writeAttribute('href', null);

	// Creates an input field where users can edit the title
	let textBox =
		new Element('input', { 'type': 'text', 'value' : element.innerHTML, 'class': 'titleEditor'})
		.observe('focusout', function(e) {

			// Get the suggested new title 
			let newTitle = textBox.value;

			// Trim the value for whitespace. If the trim function doesen't exsist, use the custom function.
			// Only used to test for empty input.
			let newTitleTrimmed = (newTitle.trim) ? newTitle.trim() : newTitle.replace(/^\s+/,'');

			// If any changes were made	
			if (newTitleTrimmed && newTitle !== oldTitle) {
				
				// Update display title
				element.update(newTitle);

				// Save edited content
				saveEdits(element.up(elementsSelector), newTitle, null, function() {
					element.update(oldTitle);
				});
			}

			// Makes the content title un-editable again
			textBox.remove();
			element.show();
			
			// Re-add the parent href
			if (parentHref && parentHref != 'href') {
				parent.writeAttribute('href', parentHref);
			}
		});

	// Hide the title element, and insert the textbox after it
	// Also prevent default default enter keypress, to avoid following navigation links in titles
	element.hide()
		.up()
		.insertBefore(textBox, element.nextSibling)
		.observe('keypress', function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();				
			};
		});

	// Focus on the title
	textBox.focus();
}

function editBody(element) {
	// Ensure we get the entire body 
	if (!element.hasClassName('vtbegenerated')) {
		element = element.up('div[class*="vtbegenerated"]');
	}

	let oldContent = element.innerHTML;

	// Makes the content area editable and adds events
	element.writeAttribute('contenteditable', 'true').observe('focusout', function(e) {
		
		// Get the new value for the body
		let newBody = element.innerHTML;

		// Trim the value for whitespace. If the trim function doesen't exsist, use the custom function.
		// Only used to test for empty input.
		let newBodyTrimmed = element.innerText;
		newBodyTrimmed = (newBodyTrimmed.trim) ? newBodyTrimmed.trim() : newBodyTrimmed.replace(/^\s+/,'');

		// If any changes were made	
		if (newBodyTrimmed && newBodyTrimmed != "" && newBody !== oldContent) {
			// Save edited content
			saveEdits(element.up(elementsSelector), null, newBody, function() {
				// Restore old content if changes aren't saved
				element.update(oldContent);
			});
		} else {
			// Restore old content if no changes are going to be saved
			element.update(oldContent);
		}

		// Makes the content area un-editable again
		element.writeAttribute('contenteditable', null);
	});

	// Focus on the content area
	element.focus();
}

function saveEdits(listElement, newTitle, newBody, errorCallback) {

	doAuthorizedCall(function (token) {
		var url = '//' + window.location.host + '/learn/api/public/v1/courses/' + courseId + '/contents/' + listElement.id.replace('contentListItem:', '');

		var request = new XMLHttpRequest();

		request.open('PATCH', url);
		request.setRequestHeader('Content-Type', 'application/json');
		request.setRequestHeader('Authorization', 'Bearer ' + token);

		request.onload = function() {
			switch (request.status) {
				case 200:
					// Succes!
				break;
				default:
					alert('Something went wrong when saving your edits. Try again, or contact your local system administrator.');

					// Caller specific error handling 
					errorCallback();
				break;
			}
		};

		var msg = {};

		if (newTitle) {
			msg.title = newTitle;
		}

		if (newBody) {
			msg.body = newBody;
		}

		request.send(JSON.stringify(msg));
	});
}

addStyle(editContentDirectlyStyles);
addFix(addEditFeatures, 'listContentEditable.jsp');