/*
What does this fix do?
making content items available/unavailable is a cumbersome process
if possible (if no "advanced" availability rules are set), making items unavailable should be a one click process

SOLUTION
*/

// jscs:disable requireCamelCaseOrUpperCaseIdentifiers, maximumLineLength

var addFix = require('utilities/addFix');
var language = require('utilities/language');
var buttonStyle = require('./itemAvailabilityButtonStyles');
var getElements = require('utilities/getElements');
var getElement = require('utilities/getElement');
var courseId = require('utilities/courseId');
var addStyle = require('utilities/addStyle');
var addUserAction = require('utilities/addUserAction');
var doAuthorizedCall = require('utilities/doAuthorizedCalls');
var checkContentType = require('utilities/checkContentTypeSupported');

const elementsSelector = 'ul#content_listContainer > li[id*="contentListItem:"]';

var addAvailabilityButtons = function addAvailabilityButtons() {

  getElements(elementsSelector).each(function(item) {

    checkContentType(item, function() {

      getAvailabilityState(item, function() {

        // new buttons are created
        var newButton = {};
        newButton.class = 'avlToggle makeAvailable fas';
        newButton.title_nl = 'Maak dit item beschikbaar voor studenten.';
        newButton.title_en = 'Make this item available to students.';
        newButton.title_da = 'Gør elementet tilgængeligt for studerende.';
        addUserAction(item, newButton);
        newButton.class = 'avlToggle makeUnavailable fas';
        newButton.title_nl = 'Maak dit item onbeschikbaar voor studenten.';
        newButton.title_en = 'Make this item unavailable to students.';
        newButton.title_da = 'Gør elementet utilgængeligt for studerende.';
        addUserAction(item, newButton);

      });

    });

  });

  function getAvailabilityState(item, callback) {

    var url = '//' + window.location.host + '/learn/api/public/v1/courses/' + courseId + '/contents/' + item.id.replace('contentListItem:', '') + '?fields=availability';

    var request = new XMLHttpRequest();

    request.open('GET', url);
    request.setRequestHeader('Content-Type', 'application/json');

    request.onload = function() {
      switch (request.status) {
        case 200:
          let response = JSON.parse(request.responseText);
          let available = response.availability.available;

          let startDateTime = new Date(response.availability.adaptiveRelease.start);
          let endDateTime = new Date(response.availability.adaptiveRelease.end);
          let nowToday = new Date();

          item.addClassName(available === 'Yes' ? 'itemIsAvailable' : 'itemIsUnavailable');

          if ((startDateTime && startDateTime > nowToday) || (endDateTime && endDateTime <= nowToday)) {
            item.addClassName('outsideAvaliabilityTime');
          }
          
          callback();
        break;
        default:
          console.log('Something went wrong trying to get a content item´s availability. Try refreshing the page, or contact your local system administrator.');
        break;
      }
    };

    request.send();
  }

  var showNewState = function(toggledItem) {
    var unavailableLabel = {
      'en': 'Availability',
      'nl': 'Beschikbaarheid',
      'da': 'Tilgængelighed'
    };
    var unavailableText = {
      'en': 'Item is not available.',
      'nl': 'Item is niet beschikbaar.',
      'da': 'Elementet er ikke tilgængeligt.'
    };

    var thisItem = getElement(toggledItem);
    var thisItemIcon = thisItem.down('.item_icon');

    if (thisItem.hasClassName('itemIsUnavailable')) {
      // remove unavailable text
      thisItem.down('.contextItemDetailsHeaders').remove();

      if (!thisItem.hasClassName('outsideAvaliabilityTime')) {
        // change icon
        thisItemIcon.src = thisItemIcon.src.replace('_non.', '_on.');
      }

      // change item's class
      thisItem.removeClassName('itemIsUnavailable');
      thisItem.addClassName('itemIsAvailable');
    } else {
      // add unavailable text
      thisItem.down('.details').insert({
        top: '<div class="contextItemDetailsHeaders clearfix"><div class="detailsLabel u_floatThis-left">' + unavailableLabel[language] + ':</div><div class="detailsValue u_floatThis-left">' + unavailableText[language] + '</div></div>'
      });
      // change icon
      thisItemIcon.src = thisItemIcon.src.replace('_on.', '_non.');
      // change item's class
      thisItem.removeClassName('itemIsAvailable');
      thisItem.addClassName('itemIsUnavailable');
    }
  };

  getElement('#content_listContainer').observe('click', function(event) {
    // if contextual menu is open, do not change availability
    if (!getElement('.cmdiv') || !getElement('.cmdiv').visible()) {
      var thisItem = event.target.up('.liItem');
      if (event.target.hasClassName('avlToggle') && !thisItem.hasClassName('avlToggleBusy')) {
        var setTo = 'No';
        if (event.target.hasClassName('makeAvailable')) {
          setTo = 'Yes';
        }

        doAuthorizedCall(function (token) {
          // TODO: KR execute the same DOM search multiple times (event.target.up('li'))
          var url = '//' + window.location.host + '/learn/api/public/v1/courses/' + courseId + '/contents/' + thisItem.id.replace('contentListItem:', '');
          var toggledItem = '#' + thisItem.id;

          getElement(toggledItem).addClassName('avlToggleBusy');

          var request = new XMLHttpRequest();

          request.open('PATCH', url);
          request.setRequestHeader('Content-Type', 'application/json');
          request.setRequestHeader('Authorization', 'Bearer ' + token);

          request.onload = function() {
            switch (request.status) {
              case 200:
                showNewState(toggledItem);
                break;
              default:
                alert('Something went wrong tryin to change the availability of the content item. Refresh the page and try again, or contact your local system administrator.');
                break;
            }

            getElement(toggledItem).removeClassName('avlToggleBusy');
          };

          request.send('{ "availability": { "available": "' + setTo + '" } }');
        });
      }
    }
  });
};

addStyle(buttonStyle);
addFix(addAvailabilityButtons, 'listContentEditable.jsp', '#editModeToggleLink');