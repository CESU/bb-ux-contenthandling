var addFix = require('utilities/addFix');
var courseId = require('utilities/courseId');
var getElement = require('utilities/getElement');
var getElements = require('utilities/getElements');
var addUserAction = require('utilities/addUserAction');
var addStyle = require('utilities/addStyle');
var menuAvailabilityButtonStyles = require('./menuAvailabilityButtonsStyles');

const elementsSelector = 'body[class*="ineditmode"] ul#courseMenuPalette_contents > li[id*="paletteItem:"]:not(.divider):not(.subhead)';

var addMenuAvailabilityButtons = function () {

	// modify display of items
	// add button set to content items
	getElements(elementsSelector).each(function(item) {

		addUserAction(item, {
			'class': 'menuAvailabilityBtn fas',
			'title_en': 'Click to toggle visibility of link',
        	'title_da': 'Klik for at ændre linkets synlighed'
		});

		// observe buttons
		item.down('.menuAvailabilityBtn').observe('click', function (event) {
			toggleAvailability(event.target.up('li[id*="paletteItem:"]'));
		});
		
		// Indicate if the menu item is hidden from users
		// NB! The check for .invisible can't be used, as this is also present when an item is empty
		if (item.down('.cmLink-hidden')) {
			item.addClassName('uxhidden');
		}
	});
};

// TODO: BUG: item not defined/is null
function toggleAvailability(item) {

	// Get the context dropdown and click it
	var contextDropper = item.down('a[id*="cmlink"]').simulate('focus').simulate('click');

	let contexMenu;

	while(!contexMenu) {
		// Get the resulting context menu 
		contexMenu = getElement('body div[id^="cmdiv"]');
	}

	let timer = setTimeout(function() {
		// Click the toggle visibility anchor		
		contexMenu.down('ul[id^="cmul"] > li > a[href*="toggleItemAvailability"]').simulate('click');
	}, 200);

}

addStyle(menuAvailabilityButtonStyles);
addFix(addMenuAvailabilityButtons, 'course_id=');