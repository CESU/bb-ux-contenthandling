/*
What does this fix do?
new content items are always added at the bottom of the list
many instructors want to move the newest item to the top of the list

SOLUTION
add a button to move itens to the top of the list
*/

// jscs: disable requireCamelCaseOrUpperCaseIdentifiers

var _ = require('underscore');
var addFix = require('utilities/addFix');
var moveToTopStyle = require('./moveToTopButtonStyles');
var getElements = require('utilities/getElements');
var moveItem = require('utilities/moveItem');
var addStyle = require('utilities/addStyle');
var addUserAction = require('utilities/addUserAction');

var button = {
  title_nl: 'Klik om dit item naar de eerste positie te verplaatsen',
  title_en: 'Click to move this item to the top of this page',
  title_da: 'Klik for at flytte elementet til toppen af siden',
  class: 'moveToTop  fas'
};

var addMoveToTopButtons = function addMoveToTopButtons() {
  addStyle(moveToTopStyle);
  getElements('#content_listContainer .liItem').each(function(contentItem) {
    addUserAction(contentItem, button);
    contentItem.down('.moveToTop').observe('click', _.partial(moveItem, contentItem, 0));
  });
};

addFix(addMoveToTopButtons, 'listContentEditable.jsp', '#editModeToggleLink');
