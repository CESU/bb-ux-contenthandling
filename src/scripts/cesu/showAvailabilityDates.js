var addFix = require('utilities/addFix');
var courseId = require('utilities/courseId');
var getElements = require('utilities/getElements');
var locale = require('utilities/locale');
var language = require('utilities/language');

var showAvailabilityDates = function () {

	function getAvailabilityDates(item) {

		var url = '//' + window.location.host + '/learn/api/public/v1/courses/' + courseId + '/contents/' + item.id.replace('contentListItem:', '') + '?fields=availability';

		var request = new XMLHttpRequest();

		request.open('GET', url);
		request.setRequestHeader('Content-Type', 'application/json');

		request.onload = function() {
			switch (request.status) {
				case 200:
					var response = JSON.parse(request.responseText);
					var startDateTime = response.availability.adaptiveRelease.start;					
					var endDateTime = response.availability.adaptiveRelease.end;

					var anchor = item.down('.vtbegenerated');
					var position = 'before';

					var formatOptions = { day: 'numeric', month: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric' }; 

					if (!anchor) {
						anchor = item.down('.details');
						position = 'bottom';
					}

					if (startDateTime || endDateTime) {
						var textAvaliable = (language === 'da') ? 'Tilgængelig' : 'Avaliable';
						var textFrom = (language === 'da') ? 'fra ' : 'from ';
						var textTo = (language === 'da') ? 'indtil ' : 'till ';

						var availabilityDateText = 
							`<div class="contextItemDetailsHeaders clearfix">
								<div class="detailsLabel u_floatThis-left">` +
									textAvaliable +
								`</div>` +

								(startDateTime ? 
								'<div class="detailsValue u_floatThis-left">' +
									textFrom + '<time>' + new Date(startDateTime).toLocaleString(locale.replace('_', '-'), formatOptions) + '</time>' +
								'</div>' : '') +
								(endDateTime ? 
								'<div class="detailsValue u_floatThis-left">' +
									textTo + '<time>' + new Date(endDateTime).toLocaleString(locale.replace('_', '-'), formatOptions) + '</time>' +
								'</div>' : '') +
							'</div>';

						anchor.insert({[position]: availabilityDateText});
					}
				break;
				default:
					console.log('Something went wrong trying to show the availability dates of a content item. Try refreshing the page, or contact your local system administrator.');
				break;
			}
		};

		request.send();
	}

	getElements('#content_listContainer > li').each(function(li) {
		getAvailabilityDates(li);
	});
};

addFix(showAvailabilityDates, 'listContent');