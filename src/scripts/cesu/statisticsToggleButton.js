var addFix = require('utilities/addFix');
var courseId = require('utilities/courseId');
var getElement = require('utilities/getElement');
var getElements = require('utilities/getElements');
var addUserAction = require('utilities/addUserAction');
var addStyle = require('utilities/addStyle');
var statisticsToggleButtonStyles = require('./statisticsToggleButtonStyles');

const elementsSelector = 'body[class*="ineditmode"] ul#content_listContainer > li[id*="contentListItem:"]';

var addStatisticsToggleButtons = function () {

	// modify display of items
	// add button set to content items
	getElements(elementsSelector).each(function(item) {

		addUserAction(item, {
			'class': 'toggleStatisticsBtn fas',
			'title_en': 'Click to turn on/off statistics tracking for this content item',
			'title_da': 'Klik for at slå statistiksporing til eller fra for dette element'
		});

		// observe buttons
		item.down('.toggleStatisticsBtn').observe('click', function (event) {
			goToToggleStatistics(event.target.up('li'));
		});
		
		
	});
};

function goToToggleStatistics(item) {
	var url = '//' + window.location.host + '/webapps/blackboard/content/manageTracking.jsp?course_id=' + courseId + '&content_id=' +  item.id.replace('contentListItem:', '');

	window.location.href = url;
}

addStyle(statisticsToggleButtonStyles);
addFix(addStatisticsToggleButtons, 'listContentEditable.jsp');