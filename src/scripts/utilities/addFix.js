// function(fix, urlPart, selector)
//   a function which registers the specified fix (a function), to be executed
//   on all pages whose url contains the specified urlPart (a String) and which
//   contain an element matching the specified selector (a String).
//   The same fix can be registered multiple times for different urlParts and
//   selectors. If a page matches multiple of these selectors, the fix will
//   nonetheless only be executed once.
//   The urlPart and selector arguments are optional.

var urlContains = require('utilities/urlContains');
var pageContains = require('utilities/pageContains');
var FastInit = require('FastInit');
//var _ = require('underscore');

var fixes = [];
var targets = {};

// IE doesn't support the func.name property. So we use a function to extract the name.
var getFunctionName = function(func) {
  var funcNameRegex = /function\s([^(]{1,})\(/;
  var results = (funcNameRegex).exec(func.toString());
  return (results && results.length > 1) ? results[1].trim() : '';
};

var addFix = function(fix, urlPart, selector) {
  //var fixOnce = _.once(fix);
  if (FastInit.done) {
    applyFix(fix, urlPart, selector);
  } else {
    registerFix(fix, urlPart, selector);
  }
};

var registerFix = function(fix, urlPart, selector) {
  fixes.push(fix);
  var fixTargets = targets[fix];
  if (!fixTargets) {
    fixTargets = targets[fix] = [];
  }
  fixTargets.push({urlPart: urlPart, selector: selector});
};

var applyFix = function(fix, urlPart, selector) {
  if (urlContains(urlPart) && pageContains(selector)) {
    try {
      console.log('Applying fix: ', fix.name || getFunctionName(fix) || fix.toString().split('\n').slice(1, 4));
      fix();
    } catch (error) {
      if (console && console.warn) {
        console.warn('An error occurred applying a fix.\n' + error.stack + '\n' + fix.toString());
      }
    }
    return true;
  } else {
    return false;
  }
};

var applyRegisteredFixes = function() {
  fixes.uniq().forEach(function(fix) {
    var notDone = true;
    targets[fix].forEach(function(target) {
      if (notDone && applyFix(fix, target.urlPart, target.selector)) {
        notDone = false;
      }
    });
  });
};

FastInit.addOnLoad(applyRegisteredFixes);

module.exports = addFix;
