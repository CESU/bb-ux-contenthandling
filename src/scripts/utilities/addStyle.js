var getElement = require('utilities/getElement');
var htmlToElement = require('utilities/htmlToElement');

module.exports = function (template, data) {
  data = data || {};
  var styles = template(data);
  var stylesEl = htmlToElement(styles);
  getElement('head').insert({
    bottom: stylesEl
  });
  return stylesEl;
};

// experiment
/*
var addStyles = function (styles, element) {
  var styleTag = element || createStyleTag();
  styleTag.innerHTML = styles;
  return styleTag;
};

var createStyleTag = function () {
  var styleTag = new Element('style');
  getElement('head').insert({bottom: styleTag});
};
*/
