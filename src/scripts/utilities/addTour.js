/*globals introJs*/
// addTour: function (name, steps, [urlPart], [selector])
// 	a function which adds the tour with the specified name and steps to the tour which is shown on this page,
// 	if 1) this page matches the specified urlPart and selector and 2) the current user hasn't already taken the tour.
// 	The steps argument is an array of objects containing 3 properties: element, intro and position. The position property is optional.
// 	Example:
// 		{
// 			element: '#step4',
// 			intro: "Another step.",
// 			position: 'bottom'
// 		}

var _ = require('underscore');
var baseThemeUrl = require('utilities/baseThemeUrl');
var urlContains = require('utilities/urlContains');
var pageContains = require('utilities/pageContains');
var getElement = require('utilities/getElement');
var userRegistryUtils = require('utilities/userRegistryUtils');
var getProperty = userRegistryUtils.getProperty;
var setProperty = userRegistryUtils.setProperty;
var language = require('utilities/language');
var head = require('head');
var FastInit = require('FastInit');

var ONE_WEEK_IN_SECONDS = 3600 * 24 * 7;

var TOUR_PREFIX = 'toledo-tour-';

// an array containing the tours to be shown. A tour contains an tour number and steps.
// The tours will be shown, ordered on their tour number.
var tours = [];
var tourStarted = false;

var tourNumber = 0;
var getTourNumber = function () {
  tourNumber += 1;
  return tourNumber;
};

var loadIntroJsIfNeeded = _.once(function () {
  head.load([
    {'intro.js': baseThemeUrl + '/lib/introjs/intro.min.js'},
    {'introjs.css': baseThemeUrl + '/lib/introjs/introjs.min.css'}
  ]);
});

var useIntroJs = function (callback) {
  head.ready(['intro.js', 'introjs.css'], callback);
};

var addTour = function (name, steps, urlPart, selector) {
  var tourNumber = getTourNumber();
  if (urlContains(urlPart) && pageContains(selector, {visible: true})) {
    var propertyName = TOUR_PREFIX + name;
    getProperty(propertyName, {
      cache: ONE_WEEK_IN_SECONDS,
      success: function (value) {
        if (value !== 'true') {
          loadIntroJsIfNeeded();
          tours.push({tourNumber: tourNumber, steps: steps});
          if (FastInit.done) {
            startTour();
          }
          setProperty(propertyName, 'true', {cache: ONE_WEEK_IN_SECONDS});
        }
      }
    });
  }
};

var getTourSteps = function () {
  var sortedTours = tours.sortBy(function (tour) {
    return tour.tourNumber;
  });
  var tourSteps = sortedTours.map(function (tour) {
    return tour.steps;
  });
  return tourSteps.flatten();
};

var startTour = function startTour() {
  if (!tourStarted && tours.length) {
    tourStarted = true;
    useIntroJs(function () {
      var bodyEl = getElement('body');
      var intro = introJs();
      var steps = getTourSteps();
      intro.setOptions({
        steps: steps,
        doneLabel: language === 'nl' ? 'Klaar' : 'Done',
        nextLabel: language === 'nl' ? 'Volgende' : 'Next',
        prevLabel: language === 'nl' ? 'Vorige' : 'Previous',
        skipLabel: language === 'nl' ? 'Afsluiten' : 'Skip',
        exitOnOverlayClick: false,
        exitOnEsc: false
      });
      intro.onexit(function () {
        tourStarted = false;
        setTimeout(startTour, 1);
        bodyEl.removeClassName('tol-oneStepIntro');
      });
      intro.oncomplete(function () {
        tourStarted = false;
        setTimeout(startTour, 1);
        bodyEl.removeClassName('tol-oneStepIntro');
      });
      intro.onchange(function () {
        if (intro._currentStep !== steps.length - 1) {
          bodyEl.addClassName('tol-hideSkipButton');
        } else {
          bodyEl.removeClassName('tol-hideSkipButton');
        }
      });
      if (steps.length === 1) {
        bodyEl.addClassName('tol-oneStepIntro');
      }
      tours = [];
      intro.start();
    });
  }
};

FastInit.addOnLoad(startTour);

module.exports = addTour;
