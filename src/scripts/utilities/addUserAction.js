var extraStyles = require('./addUserActionStyles');
var language = require('utilities/language');
var htmlToElement = require('utilities/htmlToElement');
var addStyle = require('utilities/addStyle');

// function to add userAction-button to item/announcements
/*
  used for:
  - make item available or unavailable
  - move item up
  - insert content before or after item
  - ...
*/

module.exports = function(parentElement, button) {
  // check if styles-container exists
  if (!window.userActionStylesAdded) {
    addStyle(extraStyles);
    window.userActionStylesAdded = true;
  }
  // check if container userActions exists
  if (!parentElement.down('.userActions')) {
    parentElement.insert({
      bottom: '<div class="userActions"></div>'
    });
  }
  // insert the new button
  var newButton = htmlToElement('<a title="' + button['title_' + language] + '"class="' + button.class + '"></a>');
  parentElement.down('.userActions').insert({
    bottom: newButton
  });
  // return the new button (to attach listener)
  return newButton;
};
