
var request = function(method, url, options) {
  var contentType = options.contentType ||  'application/json';
  var success = options.success;
  var error = options.error;
  var data = options.data;
  var xhr = new XMLHttpRequest();
  xhr.open(method, url);
  xhr.setRequestHeader('Content-Type', contentType + '; charset=UTF-8');
  xhr.onload = function() {
    var response = xhr.responseText;
    var status = xhr.status;
    if (status >= 200 && status < 400) {
      if (success) {
        success(response, status, xhr);
      }
    } else {
      if (error) {
        error(xhr, status, response);
      }
    }
  };
  xhr.onerror = function() {
    if (error) {
      error(xhr);
    }
  };
  if (data) {
    xhr.send(JSON.stringify(data));
  } else {
    xhr.send();
  }
};

module.exports = {
  get: function(url, options) {
    request('GET', url, options || {});
  },
  post: function(url, options) {
    request('POST', url, options || {});
  },
  put: function(url, options) {
    request('PUT', url, options || {});
  },
  delete: function(url, options) {
    request('DELETE', url, options || {});
  }
};
