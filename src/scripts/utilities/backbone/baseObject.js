var _ = require('underscore');
var Backbone = require('utilities/prototype-backbone');
var proxyGetOption = require('./utilities').proxyGetOption;

var BaseObject = function(options) {
  this.options = _.extend({}, _.result(this, 'options'), options);

  this.initialize.apply(this, arguments);
};

BaseObject.extend = Backbone.Model.extend;

// Ensure it can trigger events with Backbone.Events
_.extend(BaseObject.prototype, Backbone.Events, {

  //this is a noop method intended to be overridden by classes that extend from this base
  initialize: function() {},

  // Proxy `getOption` to enable getting options from this or this.options by name.
  getOption: proxyGetOption

});

module.exports = BaseObject;
