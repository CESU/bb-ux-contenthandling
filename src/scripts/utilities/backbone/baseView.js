var _ = require('underscore');
var Backbone = require('utilities/prototype-backbone');
var proxyGetOption = require('./utilities').proxyGetOption;
var _getValue = require('./utilities')._getValue;
var htmlToElement = require('utilities/htmlToElement');
var addStyle = require('utilities/addStyle');

var BaseView = Backbone.View.extend({

  constructor: function(options) {
    this.render = _.bind(this.render, this);

    options = _getValue(options, this);

    // this exposes view options to the view initializer
    // this is a backfill since backbone removed the assignment
    // of this.options
    // at some point however this may be removed
    this.options = _.extend({}, _.result(this, 'options'), options);

    Backbone.View.call(this, this.options);
  },

  // Get the template for this view
  // instance. You can set a `template` attribute in the view
  // definition or pass a `template: "whatever"` parameter in
  // to the constructor options.
  getTemplate: function() {
    return this.getOption('template');
  },

  // Serialize a model by returning its attributes. Clones
  // the attributes to allow modification.
  serializeModel: function(model) {
    return model.toJSON.apply(model, _.rest(arguments));
  },

  // Serialize a collection by serializing each of its models.
  serializeCollection: function(collection) {
    return collection.toJSON.apply(collection, _.rest(arguments));
  },

  // Serialize the model or collection for the view. If a model is
  // found, the view's `serializeModel` is called. If a collection is found,
  // each model in the collection is serialized by calling
  // the view's `serializeCollection` and put into an `items` array in
  // the resulting data. If both are found, defaults to the model.
  // You can override the `serializeData` method in your own view definition,
  // to provide custom serialization for your view's data.
  serializeData: function() {
    if (!this.model && !this.collection) {
      return {};
    }

    var args = [this.model || this.collection];
    if (arguments.length) {
      args.push.apply(args, arguments);
    }

    if (this.model) {
      return this.serializeModel.apply(this, args);
    } else {
      return {
        items: this.serializeCollection.apply(this, args)
      };
    }
  },

  // Mix in template helper methods. Looks for a
  // `templateHelpers` attribute, which can either be an
  // object literal, or a function that returns an object
  // literal. All methods and attributes from this object
  // are copies to the object passed in.
  mixinTemplateHelpers: function(target) {
    target = target || {};
    var templateHelpers = this.getOption('templateHelpers');
    templateHelpers = _getValue(templateHelpers, this);
    return _.extend(target, templateHelpers);
  },

  // Render the view, defaulting to underscore.js templates.
  // You can override this in your view definition to provide
  // a very specific rendering for your view. In general, though,
  // you should override the `Marionette.Renderer` object to
  // change how Marionette renders views.
  render: function() {
    this.addStyles();

    var template = this.getTemplate();

    // Allow template-less item views
    if (template === false) {
      return;
    }

    if (!_.isFunction(template)) {
      throw new Error('Cannot render the template since it is not a function.');
    }

    // Add in entity data and template helpers
    var data = this.mixinTemplateHelpers(this.serializeData());

    // Render and add to el
    var html = template(data);
    this.attachElContent(html);

    return this;
  },

  addStyles: function () {
    var stylesTemplate = this.getOption('styles');
    if (stylesTemplate) {
      this.stylesEl = addStyle(stylesTemplate);
    }
  },

  remove: function () {
    if (this.stylesEl) {
      this.stylesEl.remove();
      delete this.stylesEl;
    }
    Backbone.View.prototype.remove.call(this);
  },

  // Attaches the content of a given view.
  // This method can be overridden to optimize rendering,
  // or to render in a non standard way.
  //
  // For example, using `innerHTML` instead of `$el.html`
  //
  // ```js
  // attachElContent: function(html) {
  //   this.el.innerHTML = html;
  //   return this;
  // }
  // ```
  attachElContent: function(html) {
    if (this.getOption('noWrap') === true) {
      this.setElement(htmlToElement(html));
    } else {
      this.el.innerHTML = html;
    }
    return this;
  },

  // Proxy `getOption` to enable getting options from this or this.options by name.
  getOption: proxyGetOption

});

module.exports = BaseView;
