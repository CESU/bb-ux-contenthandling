var _ = require('underscore');
var BaseObject = require('./baseObject');

// Region
// ------

// Manage the visual regions of your composite application. See
// http://lostechies.com/derickbailey/2011/12/12/composite-js-apps-regions-and-region-managers/

var Region = BaseObject.extend({
  constructor: function(options) {

    // set options temporarily so that we can get `el`.
    // options will be overriden by Object.constructor
    this.options = options || {};
    this.el = this.getOption('el');

    if (!this.el) {
      throw new Error('An "el" must be specified for a region.');
    }

    this.position = this.getOption('position');

    if (!_.contains(['before', 'after', 'top', 'bottom', 'update'], this.position)) {
      throw new Error('An "position" must be specified for a region. ' +
        'Valid positions are: "before", "after", "top", "bottom", "update".');
    }

    // this.$el = this.getEl(this.el);
    Object.call(this, options);
  },

  // Displays a backbone view instance inside of the region.
  // Handles calling the `render` method for you. Reads content
  // directly from the `el` attribute. Also calls an optional
  // `onShow` and `onDestroy` method on your view, just after showing
  // or just before destroying the view, respectively.
  // The `preventDestroy` option can be used to prevent a view from
  // the old view being destroyed on show.
  // The `forceShow` option can be used to force a view to be
  // re-rendered if it's already shown in the region.
  show: function(view) {
    if (!this._ensureElement()) {
      return;
    }

    var isDifferentView = view !== this.currentView;

    if (this.currentView) {
      delete this.currentView._parent;
    }

    if (isDifferentView) {
      this.empty();
    }

    if (isDifferentView) {

      // We need to listen for if a view is destroyed
      // in a way other than through the region.
      // If this happens we need to remove the reference
      // to the currentView since once a view has been destroyed
      // we can not reuse it.
      view.once('destroy', this.empty, this);

      view.render();

      this.attachHtml(view);
      this.currentView = view;

      return this;
    }

    return this;
  },

  _ensureElement: function() {
    if (!_.isObject(this.el)) {
      this.$el = this.getEl(this.el);
      this.el = this.$el;
    }

    if (!this.$el || this.$el.length === 0) {
      throw new Error('An "el" ' + this.$el.selector + ' must exist in DOM');
    }
    return true;
  },

  // Override this method to change how the region finds the DOM
  // element that it manages. Return a jQuery selector object scoped
  // to a provided parent el or the document if none exists.
  getEl: function(el) {
    return el instanceof Element ? el : $$(el)[0];
  },

  // Override this method to change how the new view is
  // appended to the `$el` that the region is managing
  attachHtml: function(view) {
    if (this.position === 'update') {
      this.$el.update(view.el);
    } else {
      var insertOptions = {};
      insertOptions[this.position] = view.el;
      this.$el.insert(insertOptions);
    }
  },

  // Destroy the current view, if there is one. If there is no
  // current view, it does nothing and returns immediately.
  empty: function() {
    var view = this.currentView;

    // If there is no view in the region
    // we should not remove anything
    if (!view) { return; }

    view.off('destroy', this.empty, this);

    view.remove();

    // Remove region pointer to the currentView
    delete this.currentView;

    return this;
  },

  // Checks whether a view is currently present within
  // the region. Returns `true` if there is and `false` if
  // no view is present.
  hasView: function() {
    return !!this.currentView;
  },

  // Reset the region by destroying any existing view and
  // clearing out the cached `$el`. The next time a view
  // is shown via this region, the region will re-query the
  // DOM for the region's `el`.
  reset: function() {
    this.empty();

    if (this.$el) {
      this.el = this.$el.selector;
    }

    delete this.$el;
    return this;
  }

});

module.exports = Region;
