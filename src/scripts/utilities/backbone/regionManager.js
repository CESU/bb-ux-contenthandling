var _ = require('underscore');
var Region = require('./region');

function RegionManager() {
  this.regions = [];
}

RegionManager.prototype.add = function (region) {
  if (!(region instanceof Region)) {
    throw new Error('Only regions can be added to this region manager.');
  }
  if (this.contains(region)) {
    throw new Error('This region manager already contains a region with id: ' + region.id);
  }
  this.regions.push(region);
};

RegionManager.prototype.remove = function (region) {
  if (!(region instanceof Region)) {
    throw new Error('Only regions can be removed to this region manager.');
  }
  this.regions = _.reject(this.regions, function (thisRegion) {
    return thisRegion.id === region.id;
  });
};

RegionManager.prototype.remove = function (regionId) {
  console.log(regionId);
};

// region is a Region object or a regionId (a string)
RegionManager.prototype.contains = function (region) {
  var regionId = region instanceof Region ? region.id : region;
  return !!_.find(this.regions, function (region) {
    return region.id === regionId;
  });
};

module.exports = RegionManager;
