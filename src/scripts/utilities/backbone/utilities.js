var _ = require('underscore');

// getOption
// --------------------

// Retrieve an object, function or other value from a target
// object or its `options`, with `options` taking precedence.
var getOption = function(target, optionName) {
  if (!target || !optionName) { return; }
  if (target.options && (target.options[optionName] !== undefined)) {
    return target.options[optionName];
  } else {
    return target[optionName];
  }
};

// Proxy `getOption`
var proxyGetOption = function(optionName) {
  return getOption(this, optionName);
};

// Similar to `_.result`, this is a simple helper
// If a function is provided we call it with context
// otherwise just return the value. If the value is
// undefined return a default value
var _getValue = function(value, context, params) {
  if (_.isFunction(value)) {
    value = params ? value.apply(context, params) : value.call(context);
  }
  return value;
};

module.exports = {
  getOption: getOption,
  proxyGetOption: proxyGetOption,
  _getValue: _getValue
};
