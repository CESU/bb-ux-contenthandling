// baseThemeUrl: String
// 	the BaseURL of the current theme.

var getElement = require('utilities/getElement');

var getBaseThemeUrl = function () {
  var themeCssLink = getElement('head link[href*="/branding/theme"][href*="theme.css"]');
  var themeCssUrl = themeCssLink.readAttribute('href');
  var baseThemeUrl = themeCssUrl.substr(0, (themeCssUrl.indexOf('/theme.css')));
  return baseThemeUrl;
};

module.exports = getBaseThemeUrl();
