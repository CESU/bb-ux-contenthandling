var courseId = require('utilities/courseId');

// List of avaliable content-handlers
const whiteListContentTypes = 'resource/x-bb-document;resource/x-bb-externallink;resource/x-bb-folder;resource/x-bb-courselink;resource/x-bb-forumlink;resource/x-bb-blti-link;resource/x-bb-file;resource/x-bb-asmt-test-link;';

// Checks if the contenthandler of the given element is contained in the white list. If it is, the callback function is called with the element as a parameter.
var checkContentType = function(element, callback) {
	let url = '//' + window.location.host + '/learn/api/public/v1/courses/' + courseId + '/contents/' + element.id.replace('contentListItem:', '') + '?fields=contentHandler';

	let request = new XMLHttpRequest();

	request.open('GET', url);
	request.setRequestHeader('Content-Type', 'application/json');

	request.onload = function() {
		switch (request.status) {
			case 200:
				let response = JSON.parse(request.responseText);
				
				if (whiteListContentTypes.indexOf(response.contentHandler.id) !== -1) {
					callback(element);
				}				
			break;
			default:
				//alert('Something went wrong. Try again, or contact your local system administrator.');
			break;
		}
	};

	request.send();
}

module.exports = checkContentType;