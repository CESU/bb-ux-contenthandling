var queryParameter = require('utilities/queryParameter');

var urlContentId = queryParameter('content_id');

// catch double occurence of content id in url
var contentId = urlContentId ? [urlContentId].flatten()[0] : undefined;

module.exports = contentId;
