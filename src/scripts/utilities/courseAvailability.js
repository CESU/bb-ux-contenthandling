// This variable is available at the time theme.js is loaded
var {availability} = require('utilities/courseInfo');

module.exports = availability;
