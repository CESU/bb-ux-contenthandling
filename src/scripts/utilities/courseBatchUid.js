// This variable is available at the time theme.js is loaded
var {batchUid} = require('utilities/courseInfo');

module.exports = batchUid;
