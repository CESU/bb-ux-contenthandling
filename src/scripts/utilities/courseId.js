var queryParameter = require('utilities/queryParameter');

var courseId = queryParameter('course_id');

module.exports = courseId;
