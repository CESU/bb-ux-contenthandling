// Course info available at the time theme.js is loaded

var queryParameter = require('utilities/queryParameter');

var courseId = queryParameter('course_id');
var batchUid = window.tolCourseId;
var availability =  window.tolCourseAvailable;
var locale = window.tolCourseLocale || 'en_US';
var language = locale.substr(0, 2).toLowerCase();

var title = window.tolCourseTitle;
// NOTE: KR if it turns out that the tolCourseTitle variable isn't always set correctly, we can fallback to BB's
//       courseTitle variable
// NOTE: KR the tolCourseTitle variable is set BEFORE the theme.js is loaded. BB's courseTitle variable is set AFTER
//       the theme.js is loaded. This means that you MUST use addFix in order to use BB's courseTitle, but you can
//       use tolCourseTitle anywhere.

function isYearlyCommunity(batchUid) {
  return batchUid.indexOf('-K-') !== -1;
}

function isYearlessCommunity(batchUid) {
  return batchUid.slice(-2) === '-K';
}

function isCommunity(batchUid) {
  return isYearlyCommunity(batchUid) || isYearlessCommunity(batchUid);
}

function isBCourse(batchUid) {
  return batchUid.indexOf('-B-') !== -1;
}

function isXCourse(batchUid) {
  return batchUid.indexOf('-X-') !== -1;
}

function getCourseType(batchUid) {
  if (isCommunity(batchUid)) {
    return 'Community';
  } else if (isBCourse(batchUid)) {
    return 'BCourse';
  } else if (isXCourse(batchUid)) {
    return 'XCourse';
  } else {
    return 'Course';
  }
}

var type = batchUid ? getCourseType(batchUid) : undefined;

module.exports = {
  availability: availability,
  batchUid: batchUid,
  id: courseId,
  language: language,
  locale: locale,
  title: title,
  type: type
};
