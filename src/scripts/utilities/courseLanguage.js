// This variable is available at the time theme.js is loaded
var {language} = require('utilities/courseInfo');

module.exports = language;
