// This variable is available at the time theme.js is loaded
var {locale} = require('utilities/courseInfo');

module.exports = locale;
