/* globals page */

var getElement = require('utilities/getElement');

module.exports = function () {
  if (page.PageMenuToggler) {
    page.PageMenuToggler.prototype.expandOnce = page.PageMenuToggler.prototype.expandOnce || function () {
      this.menuContainerDiv.show();
      this.puller.removeClassName('pullcollapsed');
      this.contentPane.removeClassName('contcollapsed');
      this.navigationPane.removeClassName('navcollapsed');

      this.isMenuOpen = true;

      var msg = page.bundle.messages['coursemenu.hide'];
      this.menuPullerLink.title = msg;
      getElement('#expander').alt = msg;

      this.notifyToggleListeners(true);
    };

    page.PageMenuToggler.prototype.collapseOnce = page.PageMenuToggler.prototype.collapseOnce || function () {
      this.menuContainerDiv.hide();
      this.puller.addClassName('pullcollapsed');
      this.contentPane.addClassName('contcollapsed');
      this.navigationPane.addClassName('navcollapsed');

      this.isMenuOpen = false;

      var msg = page.bundle.messages['coursemenu.show'];
      this.menuPullerLink.title = msg;
      getElement('#expander').alt = msg;

      this.notifyToggleListeners(false);
    };
  }
};
