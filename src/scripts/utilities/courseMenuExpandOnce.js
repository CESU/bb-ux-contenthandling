/* globals page */

module.exports = function () {
  if (page.PageMenuToggler) {
    page.PageMenuToggler.prototype.expandOnce();
  }
};
