// This variable is available at the time theme.js is loaded
var {title} = require('utilities/courseInfo');

module.exports = title;
