// This variable is available at the time theme.js is loaded
var {type} = require('utilities/courseInfo');

module.exports = type;
