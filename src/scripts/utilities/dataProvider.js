// TOLEDO.DataProvider.get: function (options)
//   a function which loads JSON data from the server and executes callbacks on the response.
//   options:
//     url: string
//       the url to load the data from
//     cache: boolean (optional)
//       flag indicating whether or not the data is to be cached (default: false)
//       If true and cached data exists, the cached data will be used.
//       If true and no cached data exists, the data will be loaded and added to the cache.
//       If false, the cache is not used, nor updated.
//     success: function (response)
//       executed when the data is successfully loaded
//     error: function (response)
//       executed when an error occurred loading the data
//     complete: function (response)
//       executed when the request finishes (irrespective of the result)

var isFunction = function (functionToCheck) {
  var getType = {};
  return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
};

var getting = false;

var successCallbacks = [];
var errorCallbacks = [];
var completeCallbacks = [];

var cache = {};

var handleCallbacks = function (callbacks, response) {
  callbacks.forEach(function (callback) {
    try {
      callback(response);
    } catch (error) {
      if (console && console.log) {
        console.log('An error occurred during one of the callbacks.');
      }
    }
  });
};

var resetCallbacks = function () {
  successCallbacks = [];
  errorCallbacks = [];
  completeCallbacks = [];
};

var get = function (options) {
  if (!options.url) {
    throw Error('DataProvider.get() requires an "url" option.');
  }
  if (isFunction(options.success)) {
    successCallbacks.push(options.success);
  }
  if (isFunction(options.error)) {
    errorCallbacks.push(options.error);
  }
  if (isFunction(options.complete)) {
    completeCallbacks.push(options.complete);
  }

  if (options.cache) {
    var cachedResponse = cache[options.url];
    if (cachedResponse) {
      handleCallbacks(successCallbacks, cachedResponse);
      handleCallbacks(completeCallbacks, cachedResponse);
      resetCallbacks();
      return;
    }
  }

  if (!getting) {
    getting = true;
    new Ajax.Request(options.url, {
      method: 'get',
      dataType: 'json',
      onSuccess: function (response) {
        handleCallbacks(successCallbacks, response);
        if (options.cache) {
          cache[options.url] = response;
        }
      },
      onFailure: function (response) {
        handleCallbacks(errorCallbacks, response);
      },
      onComplete: function (response) {
        handleCallbacks(completeCallbacks, response);
        resetCallbacks();
        getting = false;
      }
    });
  }
};

// Make dataProvider globally accessible
var TOLEDO = window.TOLEDO = window.TOLEDO || {};
var dataProvider = TOLEDO.dataProvider = TOLEDO.dataProvider || {get: get};

// export the dataProvider
module.exports = dataProvider;
