// This script handles oAuth2, before executing the provided callback with the oAuth2 token as a parameter

// Requirements
var language = require('utilities/language');

// oAuth2 authorization token
var token;

// REST API information
var clientId = '5e038037-2cb2-4062-9958-3c58ef28a2e7';
var clientSecret = 'TJHTnCFl1MRZ0yqOdD7Aiee0ab3SA8ja';

function getoAUTH2Token(callback) {
	let url = '//' + window.location.host + '/learn/api/public/v1/oauth2/token';
	let xhr = new XMLHttpRequest();

	xhr.open('POST', url);

	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.setRequestHeader('Authorization', 'Basic ' + window.btoa(clientId + ':' + clientSecret));

	xhr.onreadystatechange=function(e) {
		if (xhr.readyState==4) {        
			switch (xhr.status) {
				case 200:
					token = JSON.parse(xhr.responseText).access_token;

					setTimeout(function() {
							token = '';
					}, JSON.parse(xhr.responseText).expires_in*1000);

					callback(token);
				break;
				default:
					alert('Something went wrong during authorization. Reload the page, or contact your local system administrator.');
					//alert(e.target.responseText);
				break;
			}
		}
	}

    xhr.send('grant_type=client_credentials');
}

module.exports = function doAuthorizedCalls(callback) {
	if (!token) {
		// Get the code that denotes the user's consent to using the rest api on their behalf		
		getoAUTH2Token(callback);
	} else {
		callback(token);
	};
} 