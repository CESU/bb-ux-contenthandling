// set context variables
//   enabled = true/false
//   suffix = string that will be added to item name to flag it as embedded

var embedCourseLinks = {enabled: true, suffix: ' <span class="embeddedCourseLink"></span>'};

module.exports = {
  enabled: embedCourseLinks.enabled,
  suffix: embedCourseLinks.suffix
};
