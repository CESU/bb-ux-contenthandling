/*jshint eqnull:true*/

module.exports = function(value) {
  if (value == null) {
    throw new Error('A required value is missing.');
  } else {
    return value;
  }
};
