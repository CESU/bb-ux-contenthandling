/*jshint maxcomplexity:false*/
// Expose the require function, to allow requiring dependencies from the browser.
// TODO: KR This should only be done for dev builds.

window.require = function (dependency) {
  switch (dependency) {
    case 'cookies-js': return require('cookies-js');
    case 'head': return require('head');
    case 'underscore': return require('underscore');
    case 'utilities/addFix': return require('utilities/addFix');
    case 'utilities/addStyle': return require('utilities/addStyle');
    case 'utilities/addTour': return require('utilities/addTour');
    case 'utilities/backbone/baseObject': return require('utilities/backbone/baseObject');
    case 'utilities/backbone/baseView': return require('utilities/backbone/baseView');
    case 'utilities/backbone/multiRegion': return require('utilities/backbone/multiRegion');
    case 'utilities/backbone/region': return require('utilities/backbone/region');
    case 'utilities/backbone/regionManager': return require('utilities/backbone/regionManager');
    case 'utilities/backbone/utilities': return require('utilities/backbone/utilities');
    case 'utilities/baseThemeUrl': return require('utilities/baseThemeUrl');
    case 'utilities/contentId': return require('utilities/contentId');
    case 'utilities/courseAvailability': return require('utilities/courseAvailability');
    case 'utilities/courseBatchUid': return require('utilities/courseBatchUid');
    case 'utilities/courseId': return require('utilities/courseId');
    case 'utilities/courseInfo': return require('utilities/courseInfo');
    case 'utilities/courseLanguage': return require('utilities/courseLanguage');
    case 'utilities/courseLocale': return require('utilities/courseLocale');
    case 'utilities/courseMenuAddToggle': return require('utilities/courseMenuAddToggle');
    case 'utilities/courseMenuCollapseOnce': return require('utilities/courseMenuCollapseOnce');
    case 'utilities/courseMenuExpandOnce': return require('utilities/courseMenuExpandOnce');
    case 'utilities/courseTitle': return require('utilities/courseTitle');
    case 'utilities/courseType': return require('utilities/courseType');
    case 'utilities/dataProvider': return require('utilities/dataProvider');
    case 'utilities/decimalSeparator': return require('utilities/decimalSeparator');
    case 'utilities/embedCourseLinks': return require('utilities/embedCourseLinks');
    case 'utilities/ensure': return require('utilities/ensure');
    case 'utilities/getElement': return require('utilities/getElement');
    case 'utilities/getElements': return require('utilities/getElements');
    case 'utilities/getVtbeContent': return require('utilities/getVtbeContent');
    case 'utilities/goTo': return require('utilities/goTo');
    case 'utilities/htmlToElement': return require('utilities/htmlToElement');
    case 'utilities/htmlToElements': return require('utilities/htmlToElements');
    case 'utilities/ifElement': return require('utilities/ifElement');
    case 'utilities/i18n': return require('utilities/i18n');
    case 'utilities/language': return require('utilities/language');
    case 'utilities/linkTracker': return require('utilities/linkTracker');
    case 'utilities/load': return require('utilities/load');
    case 'utilities/locale': return require('utilities/locale');
    case 'utilities/logExceptions': return require('utilities/logExceptions');
    case 'utilities/moveItem': return require('utilities/moveItem');
    case 'utilities/pageContains': return require('utilities/pageContains');
    case 'utilities/prototype-backbone': return require('utilities/prototype-backbone');
    case 'utilities/queryParameter': return require('utilities/queryParameter');
    case 'utilities/removeElement': return require('utilities/removeElement');
    case 'utilities/removeElements': return require('utilities/removeElements');
    case 'utilities/urlContains': return require('utilities/urlContains');
    case 'utilities/userBatchUid': return require('utilities/userBatchUid');
    case 'utilities/userEmail': return require('utilities/userEmail');
    case 'utilities/userFamilyName': return require('utilities/userFamilyName');
    case 'utilities/userGivenName': return require('utilities/userGivenName');
    case 'utilities/userLanguage': return require('utilities/userLanguage');
    case 'utilities/userLocale': return require('utilities/userLocale');
    case 'utilities/userInfo': return require('utilities/userInfo');
    case 'utilities/userRegistryUtils': return require('utilities/userRegistryUtils');
    case 'utilities/whenElement': return require('utilities/whenElement');
    case 'utilities/ajax': return require('utilities/ajax');
  }
};
