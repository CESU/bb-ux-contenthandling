// getElement: function (selector, [options])
//   a function which returns the element matching the specified selector. If more than one element matches, the first one is returned.
//   valid options are:
//     visible: if true, only visible elements are returned.

var getElements = require('utilities/getElements');

module.exports = function (selector, options) {
  var elements = getElements(selector, options);
  return elements.length ? elements[0] : undefined;
};
