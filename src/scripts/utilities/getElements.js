// getElements: function (selector, [options])
//   a function which returns an array containing all elements matching the specified selector.
//   valid options are:
//     visible: if true, only visible elements are returned.

var cache = {};
var cacheDurationInMillis = 1000;

module.exports = function (selector, options) {
  var visible = options && options.visible;
  var elements = cache[selector];
  if (!elements) {
    // If the selector is an ID-selector, use $, otherwise use $$. This is necessary because BB uses '.' in ID's, which causes $$ to fail.
    try {
      if (!selector.include(' ') && selector.startsWith('#')) {
        var elementById = $(selector.substr(1));
        if (elementById) {
          elements = [elementById];
        } else {
          elements = [];
        }
      } else {
        elements = $$(selector);
      }
    } catch (error) {
      // pretend the element doesn't exist.
      elements = [];
    }
    cache[selector] = elements;
    setTimeout(function () {
      cache[selector] = undefined;
    }, cacheDurationInMillis);
  }
  if (visible) {
    // http://makandracards.com/makandra/1339-check-whether-an-element-is-visible-or-hidden-with-javascript
    return elements.filter(function (element) {
      return element.offsetWidth > 0 && element.offsetHeight > 0;
    });
  } else {
    return elements;
  }
};
