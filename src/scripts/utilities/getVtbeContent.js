// getVtbeContent: function (editorId)
//   a function which returns the html content of the VTBE with the specified editorId.
//
//   The editorId of a VTBE is the value of the id attribute of the text-area backing the VTBE.

/* globals tinymce */

module.exports = function getVtbeContent(editorId) {
  return tinymce.editors[editorId].getContent();
};