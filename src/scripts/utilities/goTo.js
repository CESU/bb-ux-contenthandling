// goTo: function (url)
//   a function which causes the specified url to load (can be both absolute and relative).

module.exports = function (url) {
  document.location.href = url;
};
