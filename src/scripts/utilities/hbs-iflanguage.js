var Handlebars = require('hbsfy/runtime');
var language = require('utilities/language');

Handlebars.registerHelper('iflanguage', function (chosenLanguage, options) {
  if (arguments.length < 2) {
    throw new Error('Handlebars Helper iflanguage needs 1 parameters');
  }
  if (chosenLanguage === language) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});
