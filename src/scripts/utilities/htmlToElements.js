module.exports = function (html) {
  var container = new Element('div');
  container.update(html);
  return container.childElements();
};
