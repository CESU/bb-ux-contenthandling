// locale: String
// 	the locale of the currently logged in user (Defaults to 'en_US' if something goes wrong).
// language: String
// 	the language of the currently logged in user (Defaults to 'en' if something goes wrong).
// decimalSeparator: String
//  the decimal separator of the language of the currently logged in user (Defaults to '.' if something goes wrong).

var LOCALE_SETTINGS = require('LOCALE_SETTINGS') || {};
var decimalSeparator = LOCALE_SETTINGS['number_format.decimal_point'] || '.';

var locale = document.documentElement.lang;
locale = locale.startsWith('da') ? locale : 'en_UK';

module.exports = {
  locale: locale,
  language: locale.substr(0, 2),
  decimalSeparator: decimalSeparator
};
