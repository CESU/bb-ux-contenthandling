// ifElement: function (selector, [options])
//   a function which returns a function which takes another function as it's
//   argument and which executes this function when the selector, specified as
//   the argument of the first function is present on the page.
//   The same options can be passed to this function as to the getElement function.
//
// Usage:
//   ifElement('#inlineReceipt_good')(element => element.hide())
//
// NOTE:
//   This utility function is intended to replace the following construct:
//     if (getElement('#inlineReceipt_good')) {
//       getElement('#inlineReceipt_good').hide();
//     }

var getElement = require('utilities/getElement');

module.exports = function (selector, options) {
  var element = getElement(selector, options);
  return function (action) {
    if (element) {
      action(element);
    }
  };
};
