/*
Example usage:
  new LinkTracker('bbcloud-myProfile', '#BB-CORE_____myProfile a').enable();
This will track all clicks on links which matches the '#BB-CORE_____myProfile a' selector.
The first argument ('bbcloud-myProfile') is the name which will be logged for those clicks.

NOTE: If a LinkTracker is created for a link which is not present on the page, an error will be thrown.
      So create linkTrackers using a addFix function which uses the same selector as the linkTrackers themselves.
*/

var _ = require('underscore');
var getElement = require('utilities/getElement');
var userBatchUid = require('utilities/userBatchUid');

var LinkTracker = function LinkTracker(name, selector) {
  if (!name) {
    throw new Error('A LinkTracker must have a name.');
  }
  this.name = name;
  this.el = _.isElement(selector) ? selector : getElement(selector);
  if (!this.el) {
    throw new Error('Invalid selector');
  }
};

LinkTracker.prototype.enable = function () {
  this.clickHandler = this.el.on('click', _.debounce(this.track.bind(this), 200));
};

LinkTracker.prototype.disable = function () {
  if (this.clickHandler) {
    this.clickHandler.stop();
  }
};

LinkTracker.prototype.track = function () {
  var params = {
    type: 'LinkTracker',
    name: _.result(this, 'name'),
    user: userBatchUid,
    timestamp: new Date().getTime()
  };
  var loggerImage = new Image();
  loggerImage.src = '/vi/images/transparent.gif?' + Object.toQueryString(params);
};

module.exports = LinkTracker;
