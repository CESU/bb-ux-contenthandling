var _ = require('underscore');
var head = require('head');
var uuid = require('uuid');

var toHeadResources = function (resources) {
  if (!resources || !(_.isArray(resources) || _.isString(resources))) {
    return [];
  } else {
    resources = _.isString(resources) ? [resources] : resources;
    return _.map(resources, function (resource) {
      var result = {};
      result[uuid.v4()] = resource;
      return result;
    });
  }
};

module.exports = function (resources, callback) {
  var headResources = toHeadResources(resources);
  var headResourceIds = _.map(headResources, (headResource) => _.keys(headResource)[0]);
  if (headResources.length) {
    head.load(headResources);
    head.ready(headResourceIds, callback);
  } else {
    console.log('The resources parameter should contain a String or an Array of Strings.');
  }
};
