// moveItem
//   function used to move content item in content area
//    element = id of element to be moved
//    position = destination position of element

var Cookies = require('cookies-js');
var getElements = require('utilities/getElements');
var urlContains = require('utilities/urlContains');

module.exports = function(element, position) {

  // set parameters to be sent
  var queryString = '';
  var dndTimestamp = new Date().getTime();
  var sessionId = Cookies.get('JSESSIONID');
  var courseId = require('utilities/courseId');
  var contentId = require('utilities/contentId');

  // determine context
  var itemContext;
  var moveUrl;
  var insertBefore;
  var toBeMovedContentId;

  if (element.match('ul#content_listContainer > li.liItem')) {
    itemContext = 'contentList';
    moveUrl = '/webapps/blackboard/execute/course/coursemenu/coursecntrpaction';
    if (urlContains('execute/staffinfo')) {
      itemContext = 'contacts';
      moveUrl = '/webapps/blackboard/execute/staffinfo/reorderStaffInfo';
    }
    insertBefore = getElements('ul#content_listContainer > li.liItem')[position];
    toBeMovedContentId = element.down('.item').id;
    insertBefore.insert({before: element});
    getElements('ul#content_listContainer > li.liItem').each(function(item) {
      queryString += 'dnd_newOrder=' + item.down('.item').id + '&';
    });
  } else if (element.match('ul#announcementList > li[id*="announcementList:_"]')) {
    itemContext = 'announcementList';
    insertBefore = getElements('ul#announcementList > li[id*="announcementList:_"]')[position];
    moveUrl = '/webapps/blackboard/execute/repositionAnnouncement';
    toBeMovedContentId = element.id.replace('announcementList:', '');
    insertBefore.insert({before: element});
    queryString += 'dnd_newOrder:insertionMarker.announcementList&';
    getElements('ul#announcementList > li[id*="announcementList:_"]').each(function(item) {
      queryString += 'dnd_newOrder=' + item.id.replace('announcementList:', '') + '&';
    });
  } else if (element.match('ul#courseMenuPalette_contents > li[id*="paletteItem"]')) {
    itemContext = 'courseMenu';
    insertBefore = getElements('ul#courseMenuPalette_contents > li[id*="paletteItem"]')[position];
    moveUrl = '/webapps/blackboard/execute/course/coursemenu/repositionaction';
    toBeMovedContentId = element.id.replace('paletteItem:', '');
    insertBefore.insert({before: element});
    getElements('ul#courseMenuPalette_contents > li[id*="paletteItem"]').each(function(item) {
      queryString += 'dnd_newOrder=' + item.id.replace('paletteItem:', '') + '&';
    });
  }

  // complete request
  queryString += 'dnd_timestamp=' + dndTimestamp + '&';
  queryString += 'course_id=' + courseId + '&';

  // contentList
  if (itemContext === 'contentList') {
    queryString += 'content_id=' + contentId + '&';
    queryString += 'mode=reset&';
  }

  // courseMenu
  if (itemContext === 'courseMenu') {
    queryString += 'newWindow=false&openInParentWindow=false&';
  }

  queryString += 'dnd_itemId=' + toBeMovedContentId + '&';
  queryString += 'dnd_newPosition=' + position;
  queryString += '&sessionid=' + sessionId;

  if (insertBefore) {
    new Ajax.Request(moveUrl, {
      method: 'post',
      parameters: queryString
    });
  }

};
