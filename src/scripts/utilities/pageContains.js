// pageContains: function (query, [options])
//   a function which checks whether the current page contains one or more elements matching the specified query (a String or an Array of Strings).
//   if a falsy selector is passed, true is returned.

var getElement = require('utilities/getElement');

module.exports = function (query, options) {
  var selectors = [query].flatten();
  return !query || selectors.all(function (selector) {
    var negate = selector.startsWith('!');
    selector = negate ? selector.substring(1) : selector;
    var element = getElement(selector, options);
    return negate ? !element : !!element;
  });
};
