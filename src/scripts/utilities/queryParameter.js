// queryParameter: function (parameterName)
//   a function which returns the query parameter with the specified name.

var queryParameters = document.location.href.toQueryParams();

module.exports = function (parameterName) {
  return queryParameters[parameterName];
};
