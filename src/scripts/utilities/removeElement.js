// Removes the specified element

module.exports = function removeElement(element) {
  if (element) {
    element.remove();
  } else {
    console.log(
      'The removeElement function was called on a falsy value. This should not happen.' +
      'Please check the code using this function.'
    );
  }
};
