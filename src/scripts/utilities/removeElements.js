// Removes all elements matching the specified selectors

var _ = require('underscore');
var getElements = require('utilities/getElements');
var removeElement = require('utilities/removeElement');

module.exports = function removeElements(selectors) {
  _.chain(selectors).map(getElements).flatten().compact().value().each(removeElement);
};
