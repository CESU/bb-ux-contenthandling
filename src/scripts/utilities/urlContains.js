// urlContains: function (query)
//   a function which checks whether the URL of the current page contains the specified query (a String or Array of Strings).
//   if a falsy query is passed, true is returned.

module.exports = function (query) {
  var url = document.location.href;
  var urlParts = [query].flatten();
  return !query || urlParts.all(function (urlPart) {
    if (urlPart.startsWith('!')) {
      return !url.include(urlPart.substring(1));
    } else {
      return url.include(urlPart);
    }
  });
};
