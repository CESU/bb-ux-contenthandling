// This variable is available at the time theme.js is loaded
var {email} = require('utilities/userInfo');

module.exports = email;
