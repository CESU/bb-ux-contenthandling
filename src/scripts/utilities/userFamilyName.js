// This variable is available at the time theme.js is loaded
var {familyName} = require('utilities/userInfo');

module.exports = familyName;
