// This variable is available at the time theme.js is loaded
var {givenName} = require('utilities/userInfo');

module.exports = givenName;
