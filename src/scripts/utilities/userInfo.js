// User info available at the time theme.js is loaded

var batchUid = window.tolUserId;
var email = window.tolUserEmail;
var familyName  = window.tolUserFamilyName;
var givenName  = window.tolUserGivenName;
var locale = window.tolUserLocale || 'en_US';
var language = locale.substr(0, 2).toLowerCase();

module.exports = {
  batchUid: batchUid,
  email: email,
  familyName: familyName,
  givenName: givenName,
  language: language,
  locale: locale
};