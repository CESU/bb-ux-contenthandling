// getProperty: function (key, [options])
//   a function which retrieves the user property with the specified key from the server. If the property has been cached, the cached version is used without calling the server.
//   options:
//     success: function (value)
//       executed when the property is successfully retrieved.
//     failure: function
//       executed when an error occurred getting the property.
//     reload: boolean
//       flag indicating whether the property should be server from the server. Setting this flag to true, ignores and removes the cached value of the property.
//     cache: number
//       number indicating the number of seconds the property should be cached after it's loaded. If argument is not specified, the property is not cached.
// setProperty: function (key, value, [options])
//   a function which saves the user property with the specified key and value on the server. If the property has been cached, the cached version is also updated.
//   options:
//     success: function
//       executed when the property is successfully set.
//     failure: function
//       executed when an error occurred setting the property.
//     cache: number
//       number indicating the number of seconds the property should be cached after it's loaded. If argument is not specified, the property is not cached.

var Cookies = require('cookies-js');

var COOKIE_PREFIX = 'tol-theme-js-';

var setProperty = function (key, value, options) {
  options = options || {};
  var url = '/webapps/tol-web-rs-bb-bb_bb60/userRegistry.do?key=' + key + '&value=' + value;
  new Ajax.Request(url, {
    method: 'post',
    onSuccess: function () {
      if (options.cache) {
        Cookies.set(COOKIE_PREFIX + key, value, {expires: options.cache});
      } else {
        Cookies.expire(COOKIE_PREFIX + key);
      }
      if (options.success) {
        options.success();
      }
    },
    onFailure: function () {
      if (options.failure) {
        options.failure();
      }
    }
  });
};

var getProperty = function (key, options) {
  options = options || {};
  var cookieValue = Cookies.get(COOKIE_PREFIX + key);
  if (options.reload || !cookieValue) {
    new Ajax.Request('/webapps/tol-web-rs-bb-bb_bb60/userRegistry.do?key=' + key, {
      method: 'get',
      onSuccess: function (transport) {
        var response = transport.responseText;
        var valueObj = JSON.parse(response);
        var value = valueObj.value;
        if (options.cache) {
          Cookies.set(COOKIE_PREFIX + key, value, {expires: options.cache});
        }
        if (options.success) {
          options.success(value);
        }
      },
      onFailure: function () {
        if (options.failure) {
          options.failure();
        }
      }
    });
  } else {
    if (options.success) {
      options.success(cookieValue);
    }
  }
};

module.exports = {
  getProperty: getProperty,
  setProperty: setProperty
};
