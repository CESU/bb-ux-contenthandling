// whenElement: function (selector, [timeout], [interval]])
//   a function which returns a function which checks periadically (once each
//   specified interval, until a specified timeout) whether the DOM contains an
//   element matching the specified selector. The returned function takes a function
//   as it's argument and executes it if and when the element has been found, with
//   the found element as it's only argument.
//   The interval and timeout arguments are optional. A default interval of 0.1s
//   and timeout of 5s are used.
//
//   Usage:
//     whenElement('.portlet .countList')((element) => console.log('executing action'))

var getElement = require('utilities/getElement');

module.exports = function whenElement(selector, timeoutInSeconds = 5, intervalInSeconds = 0.1) {
  var maximumIterations = Math.ceil(timeoutInSeconds / intervalInSeconds);
  var iterationCount = 0;
  return function (action) {
    new PeriodicalExecuter(function(pe) {
      iterationCount += 1;
      var element = getElement(selector);
      if (element) {
        action(element);
        pe.stop();
      }
      if (iterationCount > maximumIterations) {
        pe.stop();
      }
    }, intervalInSeconds);
  };  
};
